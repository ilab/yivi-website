# Yivi

Yivi is an identity app that lets you log in easily and securely, share data and prove who you are. Without sharing too much about yourself. With Yivi, you take control of your data.

## Local development

For local development, run the following commands from the root directory in seperate terminals.

```bash
npm run hugo
```

This command runs the Hugo server and refreshes the page whenever you make changes.

```bash
npm run tailwind
```

This command detects when a file has been edited and (re)builds the css file accordingly.

```bash
npm run decap
```

Yivi uses DecapCMS for contentmanagement. Run this command to work with a local repository. Then visit this address <http://localhost:1313/adminnetje> to access it.

## Adding a static page

The static pages are made in a way where the admin can edit the texts in the CMS without needing to know how to dive into the code.  

Follow these steps to setup a new page:

* Edit the `static/adminnetje/config.yml` file and add a new entry to the folder collection. Use the existing entries as an example. Also check the [DecapCMS docs about widgets](https://decapcms.org/docs/widgets/) for the various fields you can use and customize.
* Create the markdown file by going to the CMS and add a new entry. You will need to run the decap script. Refresh your page if you were allready running the decap script.
  * Don't forget to set the create field to false of this new entry in the config.yml after the folder and .md file have been created. This is to prevent users in the CMS from creating multiple files of the same page since we only need one.
* Create the .html file for this page in the `themes\yivi-theme\layouts\_default\` folder.
* Tell Hugo which .html file to use with this .md file by adding the line `layout: <yourhtml>` without the .html at the end to your markdown file.
  * You could also instead add a hidden widget in your entry in the config.yml file and set the default value to that of your .html file.
* Define main in your .html file and retreive the contents for that file

Now you are ready to start styling your newly created static page!

### An example entry

Your new entry in the config file should look something like this when you have added 1 section to it.

```yaml
# ####################### #
#      HOW YIVI WORKS     #
# ####################### #
- label: "How Yivi works"
    name: "how_yivi_works" # Your html file should use this name: how_yivi_works.html
    folder: "content/how_yivi_works" # This is where the markdown file will be created
    i18n: true
    create: true # Set this field to false after the initial markdown file has been created. We only need one for each page.
    slug: "{{title}}"
    preview: false
    fields:
        # Specify the layout field to tell Hugo to target a particular template.
        - { label: "Page Layout", name: "layout", widget: "hidden", default: "how_yivi_works", i18n: true } 
        - { label: "Site title", name: "title", widget: "string", i18n: true }
        - label: "How Yivi works sections"
        name: sections
        widget: "object"
        i18n: true
        fields:
            - label: "How Yivi works"
                name: how_yivi_works
                widget: object
                i18n: true
                fields:
                    # CMS users have the option to decide wether a section should be displayed or not. Make sure that it gets checked in the code.
                    - { label: "Show on website", name: visibility, widget: boolean, default: true, i18n: true }
                    - { label: "Title", name: "title", widget: "string", i18n: true }
                    - { label: "Content", name: "content", widget: "markdown", i18n: true, minimal: true }
```

And your .html file would look something like this:

```html
{{ define "main" }}
    <!-- Get the contents for the current page -->
    {{ $pagecontent := .Params }} 
        {{ with site.GetPage "/how_yivi_works" }} 
        {{ range first 1 .Pages }} 
            {{ $pagecontent = .Params.sections }} 
        {{ end }} 
    {{ end }}

    <!-- Prepare content for next section -->
    {{ $content := $pagecontent.name_of_section_parameter }} 
    <!-- This is the check -->
    {{ if eq $content.visibility true }}
        <section class="bg-yivi-blue">
            <div class="container-col items-center text-center">
                <h1 class="text-black title-section">{{ $content.title }}</h1>
                <div class="container-markdown">{{ $content.content | markdownify }}</div>
            </div>
        </section>
    {{ end }}

    <!-- For the next section on the page do the same preperations -->
    <!-- Prepare content for next section -->
     {{ $content := $pagecontent.name_of_section_parameter }} 
     <!-- check check check -->
    {{ if eq $content.visibility true }}
        <!-- And style away! :) -->
        <section class="bg-yivi-blue">
            <div class="container-col items-center text-center">
                <h1 class="text-black title-section">{{ $content.title }}</h1>
                <div class="container-markdown">{{ $content.content | markdownify }}</div>
            </div>
        </section>
    {{ end }}
{{ end }}
```
