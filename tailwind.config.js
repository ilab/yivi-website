/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["content/**/*.md", "layouts/**/*.html", "themes/yivi-theme/content/**/*.md", "themes/yivi-theme/layouts/**/*.html"],
    theme: {
        extend: {
            colors: {
                yivi: {
                    DEFAULT: "#ba3555",
                    dark: "#e12747",
                    blue: "#dcecf5",
                },
                beige: {
                    DEFAULT: "#eae5e2",
                    light: "#f5f5f3",
                },
            },
            fontFamily: {
                opensans_regular: ["'opensans_regular'", "sans-serif"],
                opensans_bold: ["'opensans_bold'", "sans-serif"],
            },
        },
    },
    plugins: [],
};
