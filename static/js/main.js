// NAVIGATION MENU
const menuButtons = document.querySelectorAll(".nav__menu-button");
const menu = document.querySelector(".nav__menu");
const header = document.querySelector("header");

menuButtons.forEach((element) => {
    element.addEventListener("click", function () {
        menu.classList.toggle("scale-y-0");
    });
});

// Making the fixed headermenu prettier when scrolling
document.addEventListener("scroll", function () {
    if (document.documentElement.scrollTop > 0) {
        header.classList.add("shadow-lg", "sticky", "top-0");
    } else {
        header.classList.remove("shadow-lg", "sticky", "top-0");
    }
});
// END OFF NAVIGATION MENU

// ACCORDION
const accordion = document.querySelector(".accordion");
if (accordion) {
    const accordionItems = accordion.querySelectorAll("li.accordion-item");
    let openedWrapper = null;

    Array.from(accordionItems).forEach((item) => {
        const button = item.querySelector("button");
        const wrapper = item.querySelector(".wrapper");
        button.addEventListener("click", () => {
            // When a list items has been clicked and..

            // ..There is no wrapper open, open this list items wrapper.
            if (openedWrapper === null) {
                openedWrapper = wrapper;
                openedWrapper.classList.toggle("is-open");
            }
            // ..That list items wrapper is already open, close that wrapper.
            else if (wrapper === openedWrapper) {
                openedWrapper.classList.toggle("is-open");
                openedWrapper = null;
            }
            // ..There is already an open wrapper, close it and open this one instead.
            else {
                openedWrapper.classList.toggle("is-open");
                openedWrapper = wrapper;
                openedWrapper.classList.toggle("is-open");
            }
        });
    });
}
// END OF ACCORDION
