---
layout: storing_and_sharing
title: Wat kan ik opslaan en delen
sections:
  personal_data:
    title: Vul de Yivi-app met jouw gegevens
    content: Je vult jouw Yivi-app op verschillende manieren. De meest gebruikte
      manier is DigiD. Met deze tool haal je gecontroleerde persoons-en
      adresgegevens op bij de gemeente. Denk aan je naam, adres, geslacht en
      geboortedatum. Je telefoonnummer en e-mailadres vul je zelf in. Zo weten
      we 100% zeker dat jij echt jij bent. En zijn jouw gegevens betrouwbaar en
      controleerbaar. Handig voor jezelf, en ook voor bedrijven.
    visibility: true
  overview_storing_and_sharing:
    title: Overzicht van wat je kan opslaan en delen
    description: Wil je weten welke persoonsgegevens je in de Yivi-app kan zetten?
      Dan vind je hieronder een duidelijk overzicht. Bij elk persoonlijk gegeven
      lees je meer informatie over wat je ophaalt en de geldigheidsduur ervan.
    visibility: true
    personal_datas:
      - data:
          more_information_link: /more_information_on_data_retrieval_municipality
          title: Persoons- en adresgegevens via de Gemeente
          description: Vul je Yivi-app met je naam, geboortedatum, adres, postcode, plaats
            en BSN.
          label: Voeg je gegevens uit de BRP - Gemeente toe (Gemeente Nijmegen doet dit
            voor heel Nederland)
          label_website_link: https://services.nijmegen.nl/irma/gemeente/start
          imageAltText: ""
      - data:
          title: Data via iDIN
          label: Voeg je gegevens uit iDIN toe
          description: Vul je Yivi.app met je initialen, achternaam, geboortedatum,
            geslacht, adres, postcode, plaats.
          label_website_link: https://privacybydesign.foundation/uitgifte/idin/
          more_information_link: /more_information_on_data_retrieval_idin
---
