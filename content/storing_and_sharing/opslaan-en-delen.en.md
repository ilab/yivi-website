---
layout: storing_and_sharing
title: What can I store and share
sections:
  personal_data:
    title: Store your personal data in your Yivi-app
    content: There are different ways to store data in your Yivi app. Most people
      use DigiD. With this tool, you collect verified personal and address data
      from your municipality, which is then stored in the app. Your name,
      address, gender identity, date of birth, and so on. Manually enter your
      phone number and email address. This way, we can be certain you’re you.
      And that your data is reliable and verifiable. Which won’t just come in
      handy for yourself, but for companies, too.
    visibility: true
  overview_storing_and_sharing:
    title: Overview of what you can store and share
    description: Want to know what personal data you can put in the Yivi app? Then
      find a clear overview below. For each personal data you can read more
      about what you collect and its validity.
    visibility: true
    personal_datas:
      - data:
          more_information_link: /more_information_on_data_retrieval_municipality
          title: Personal and address data through the Municipality
          description: Fill your Yivi app with your name, date of birth, address,
            postcode, city and BSN.
          label: Add your data from the BRP - Municipality
          label_website_link: https://services.nijmegen.nl/irma/gemeente/start
          imageAltText: ""
      - data:
          title: Data via iDIN
          description: Fill your Yivi.app with your initials, surname, date of birth,
            gender, address, postcode, city.
          label: Add your data from iDIN
          label_website_link: https://privacybydesign.foundation/uitgifte/idin/
          more_information_link: /more_information_on_data_retrieval_idin
---
