---
layout: my_yivi_account
title: Gebruik Mijn Yivi
sections:
  my_yivi:
    title: Gebruik Mijn Yivi
    content: |-
      Bescherm en controleer jouw Yivi nóg beter met Mijn Yivi:



      * Je krijgt inzicht in jouw Yivi-app activiteiten
      * Je blokkeert jouw account bij verlies of diefstal van je telefoon
      * Je past eenvoudig je e-mailadres aan voor Mijn Yivi.
    image: /img/mijn-yivi-app.png
    imageAltText: Yivi op je mobiel
    visibility: true
  privacy_and_security:
    image: /img/oudere-dame-met-mobiel-en-laptop.jpg
    imageAltText: Mevrouw met mobiel
    title: Privacy en veiligheid
    content: Tuurlijk wil je dat je gegevens privé blijven, en dat deze goed
      beveiligd zijn. Dat is niet zo gek. Daarom zorgen wij voor een goede
      balans tussen privacy en beveiliging. We zien bijvoorbeeld wél wanneer je
      de Yivi-app gebruikt. Maar niet waar, of wat je ermee doet. Dat blijft
      voor ons een vraag en voor jou een weet.
    visibility: true
  change_or_block:
    title: Wijzig of blokkeer je account
    list:
      - item:
          title: E-mailadres vergeten?
          content: Ben je je e-mailadres vergeten waarmee in inlogt op Mijn Yivi? Pas het
            dan aan in **Mijn Yivi**.
      - item:
          title: Telefoon kwijt of gestolen?
          content: Blokkeer dan je Yivi-account via **Mijn Yivi**, zodat je zeker weet dat
            niemand erbij kan.
      - item:
          title: Meer vragen?
          content: Lees onze **veelgestelde vragen**.
    visibility: true
---
