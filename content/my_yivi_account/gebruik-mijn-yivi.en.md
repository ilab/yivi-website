---
layout: my_yivi_account
title: Use My Yivi account
sections:
  my_yivi:
    title: Use My Yivi account
    content: |-
      Protect and manage your Yivi account with My Yivi account:



      * Get an overview of your activities in the My Yivi app
      * Lost or stolen phone? Block your Yivi account
      * Forget your Yivi email address? Edit it.
    image: /img/mijn-yivi-app.png
    imageAltText: Yivi on mobile
    visibility: true
  privacy_and_security:
    title: Privacy and security
    content: Your data is private, and should be kept safe. Right? We think so too.
      That’s why we safeguard your privacy without compromising on security. For
      example, we can only see when you’ve used the Yivi app. Where and how you
      use it is only for you to know.
    image: /img/oudere-dame-met-mobiel-en-laptop.jpg
    imageAltText: Lady with mobile
    visibility: true
  change_or_block:
    list:
      - item:
          title: Forgot your email address?
          content: If you have forgotten your email address to login to Yivi than you can
            edit it at **My Yivi account**.
      - item:
          title: Lost or stolen phone?
          content: Then block your Yivi account, via **My Yivi account**, to make sure no
            one can access it.
      - item:
          title: More questions?
          content: Read our **frequently asked questions**.
    title: Change or block your account
    visibility: true
---
