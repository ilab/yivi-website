---
layout: terms_of_use
title: Terms of use
subtitle: By using our site, you show that you agree to these conditions.
title_one: Site conditions
content_one: >-
  * We are not liable for damages arising out of inaccuracies or omissions.

  * We own all the content (text and images) on this site, except where it says otherwise. We are happy for you to save, copy or share any of our site's content, but we ask that you acknowledge us as the source.

  * These conditions are governed by Dutch law. If you want to dispute anything in them, you need to take your case to a competent court in Arnhem, the Netherlands.
title_two: Yivi.app conditions
content_two: "* You'll find the Yivi.app conditions at the Terms and Conditions."
---
