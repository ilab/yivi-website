---
layout: terms_of_use
title: Gebruiksvoorwaarden
subtitle: Als je onze site bezoekt, ga je akkoord met onze site voorwaarden.
title_one: "Site voorwaarden:"
content_one: >-
  * Wij zijn niet aansprakelijk voor schade als gevolg van fouten op onze site.

  * Wij zijn eigenaar van alle content (tekst/beeld) op onze site, tenzij we iets anders vermelden. Je mag content van onze site opslaan, kopiëren of verspreiden. Wil je ons dan als bron vermelden?

  * Op onze voorwaarden is het Nederlands recht van toepassing. Ben je het daar niet mee eens? Leg het dan voor aan een bevoegde rechter in Arnhem.
title_two: Yivi.app voorwaarden
content_two: "* De gebruikersvoorwaarden staan beschreven onder de algemene voorwaarden"
---
