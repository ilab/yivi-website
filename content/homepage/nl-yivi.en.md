---
layout: index
title: In charge of your digital data
sections:
  trusted_organisation:
    logo: /img/logo-sidn.png
    title: Vertrouwde organisatie achter Yivi
    imageAltText: SIDN logo
    content: Yivi started as a research project at Radboud University, initially
      under the name IRMA. It moved to the non-profit [Privacy by Design
      foundation](https://privacybydesign.foundation/en/) in 2016, which further
      developed Yivi, jointly with [SIDN](https://www.sidn.nl/en) from 2019
      onwards.
    visibility: false
  in_control:
    subtitle: Organise your affairs digitally
    title: You decide what to share
    content: Booking an appointment with your GP? Do so by entering just your email
      address with Yivi. Voting in your municipality? Share your postal code.
      Nothing more, because that’s all they need to know. Buying cinema tickets?
      Only show that you’re over 18 – the rest is none of their business. Yivi
      makes it easy to run digital errands. And you’ll only share the absolutely
      necessary information.
    image: /img/Huisarts-mail.webp
    imageAltText: organising your affairs digitally
    visibility: true
  voting:
    subtitle: Show what matters to you
    title: Voting
    content: Ever have of those days where you’re completely swamped? Running from
      one appointment to the next. And to make things worse, now your
      municipality is asking for your thoughts on their plans for your
      neighbourhood. Quite important, but you barely have a minute to spare.
      Thankfully, you can use Yivi to quickly decide on that new charging
      station. Right from your phone.
    image: /img/digitaal-stemmen.webp
    imageAltText: digital voting
    visibility: true
  partners:
    title: You can already use Yivi with
    partner_logo:
      - image: /img/logo-sidn.png
        imageAltText: Logo voorbeeld
      - image: /img/logo-sidn.png
        imageAltText: Logo voorbeeld
      - imageAltText: Logo voorbeeld
        image: /img/logo-sidn.png
      - imageAltText: Logo voorbeeld
        image: /img/logo-sidn.png
    imageAltText: Dit moet nog aangepast worden
    visibility: false
  no_more_passwords:
    subtitle: Login without password
    title: No more passwords to remember
    content: Do I enter my email address or my name? And what was my password again?
      That long sequence or did I use a shorter one? With Yivi, signing in is a
      piece of cake. You scan the QR code with your Yivi app and securely share
      your data. No need to remember any account details and passwords.
      Easy-peasy.
    image: /img/inloggen-bestelproces.webp
    imageAltText: inloggen met Yivi
    visibility: true
  how_yivi_works:
    title: How Yivi works
    content: "Switching to something new can be complicated. That’s why we’ve made
      it as simple as we could. Here’s how you can get started with Yivi:
      download the free Yivi app, choose a PIN, enter your email, and collect
      your (Dutch citizen) data via DigiD. And you’ll be set in 5 minutes!"
    image: /img/Schermafbeelding-Yivi-nl.png
    imageAltText: Woman with Yivi app
    visibility: true
    video_url: https://www.youtube.com/watch?v=YAscjaHGI1E
  back_in_charge:
    title: In charge of your digital data
    subtitle: Your digital identity in one app
    content: During a first meeting, do you immediately share your address, or your
      bank details? No way. But online, you do this more often than you think.
      Not without risk. That is why there is Yivi. Yivi is an identity app that
      lets you log in easily and securely, share data and prove who you are.
      Without sharing too much about yourself. With Yivi, you take control of
      your data. You always see what an organisation wants to know about you and
      you decide whether to share that data. Your data are only on your mobile
      phone, safely behind a PIN code. No one is watching, not even Yivi. That
      is how safe it is.
    image: /img/vrouw-mobiel-laptop-Yivi-qr-code.webp
    imageAltText: Woman with mobile phone and laptio
    visibility: true
  yivi_benefits:
    title: Yivi benefits
    summary: >-
      Take control of your own data with Yivi, you ID app. And prevent a
      proliferation of accounts at various organisations. You protect your
      privacy by sharing fewer personal details. You’ll store your data in a
      place only you can access: your phone. It’s your data, so you’re in
      charge.


      **All You. All yours.**
    benefits:
      - benefit:
          title: In control of your data
          description: You decide what data to share and with whom
      - benefit:
          title: Login without passwords
          description: You only need a mobile phone and PIN to sign into many different
            websites.
      - benefit:
          title: Securely stored on your phone
          description: You’re the one who manages your personal data on your phone. Only
            you can access it, not even Yivi.
    visibility: true
  take_back_control:
    title: Take control of your digital data
    image: /img/jongen-met-yivi-app-1.jpg
    imageAltText: Man shows Yivi-app
    content: >-
      With Yivi, you choose which data you share, and who you share it with.
      It’s your data, so you’re in charge. On- and offline.


      **All you. All yours.**
    visibility: true
  signing_documents:
    subtitle: "Digital signing, the easy way "
    title: Digital signing
    content: Yivi also allows you to digitally sign and approve things. Your
      personal data then form a kind of digital stamp. So you can use Yivi to
      digitally sign as a citizen or approve something digitally from another
      business role.
    image: /img/digitale-ondertekening.webp
    imageAltText: digital signing
    visibility: true
  privacy_and_security:
    title: Your privacy and security are key
    content: "You decide what and with whom you share your data. So you never share
      more data than necessary. For example, if you want to order tickets for
      that exciting 18+ film, Yivi allows you to prove that you are 18 years or
      older by ticking the box, without sharing your date of birth. This
      protects your privacy. Your data are also conveniently stored in one place
      where only you can access them: your mobile phone. No one is watching you.
      Not even Yivi. Moreover, Yivi was built by SIDN, a not-for-profit
      organisation."
    visibility: true
---
