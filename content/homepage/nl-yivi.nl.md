---
layout: index
title: Zelf de baas over jouw gegevens met Yivi, jouw ID app!
sections:
  trusted_organisation:
    logo: /img/logo-sidn.png
    title: Vertrouwde organisatie achter Yivi
    imageAltText: SIDN logo
    content: Yivi is begonnen als een onderzoeksproject aan de Radboud Universiteit,
      destijds onder de naam IRMA. Yivi is in 2016 ondergebracht bij de
      non-profit [stichting Privacy by
      Design](https://privacybydesign.foundation/), die Yivi verder ontwikkeld
      heeft, vanaf 2019 samen met [SIDN](https://www.sidn.nl/).
    visibility: false
  in_control:
    subtitle: Je digitale zaken regelen
    title: Jij beslist wat je deelt
    content: Een afspraak maken bij de huisarts? Dat kan met alleen je e-mailadres
      via de Yivi-app. Stemmen bij de gemeente? Je deelt alleen je postcode.
      Meer hoeven zij niet te weten. Bioscooptickets kopen? Je laat alleen zien
      dat je ouder bent dan 18. Da’s meer dan genoeg. Met Yivi regel je al je
      digitale zaken, en deel je alleen wat nodig is.
    image: /img/Huisarts-mail.webp
    imageAltText: digitaal je zaken regelen
    visibility: true
  voting:
    subtitle: Laat zien wat jij belangrijk vindt
    title: Stemmen
    content: "Je hebt van die dagen: de agenda zit propvol. En je rent de hele dag
      heen en weer. Dan vraagt de gemeente je ook nog eens om mee te denken over
      plannen in je buurt. Geen tijd. Wel belangrijk. Gelukkig kun je met Yivi
      voortaan vanaf je mobiel bepalen of die nieuwe laadpaal er komt."
    image: /img/digitaal-stemmen.webp
    imageAltText: digitaal stemmen
    visibility: true
  partners:
    title: "Yivi kun je al gebruiken bij:"
    partner_logo:
      - image: /img/logo-sidn.png
        imageAltText: Logo voorbeeld
      - image: /img/logo-sidn.png
        imageAltText: Logo voorbeeld
      - imageAltText: Logo voorbeeld
        image: /img/logo-sidn.png
      - imageAltText: Logo voorbeeld
        image: /img/logo-sidn.png
    imageAltText: Dit moet nog aangepast worden
    visibility: false
  no_more_passwords:
    subtitle: Inloggen zonder wachtwoord
    title: Geen wachtwoordlijstjes meer
    content: Vul ik hier mijn e-mailadres in of mijn naam? En wat was ook alweer
      mijn wachtwoord? Die lange zin of had ik hier een korte variant? Met Yivi
      is inloggen een eitje. Je scant de QR-code met je Yivi-app en deelt veilig
      je gegevens. Je hoeft geen accountgegevens en wachtwoorden meer te
      onthouden. Wel zo makkelijk.
    image: /img/inloggen-bestelproces.webp
    imageAltText: inloggen met Yivi
    visibility: true
  how_yivi_works:
    title: Hoe Yivi werkt
    content: "Aan de slag gaan met iets nieuws, kan ingewikkeld zijn. Daarom hebben
      we het zo eenvoudig mogelijk gemaakt. Beginnen met Yivi gaat namelijk zo:
      download de gratis Yivi-app, kies een eigen pincode en vul je e-mailadres
      in, haal je gegevens op via DigiD. En… binnen 5 minuten ben je klaar."
    image: /img/Schermafbeelding-Yivi-nl.png
    imageAltText: Vrouw met Yivi app
    visibility: true
    video_url: https://www.youtube.com/watch?v=Fzcc6cEFp8w
  back_in_charge:
    title: Zelf de baas over jouw gegevens
    subtitle: Jouw digitale identiteit in één app
    content: Deel jij tijdens een eerste ontmoeting gelijk je adres, of je
      bankgegevens? Nee toch. Maar online doe je dit vaker dan je denkt. Niet
      zonder risico. Daarom is er Yivi. Yivi is een identiteitsapp waarmee je
      makkelijk en veilig inlogt, gegevens deelt en bewijst wie je bent. Zonder
      dat je te veel over jezelf deelt. Met Yivi pak jij de controle over jouw
      gegevens. Je ziet altijd wat een organisatie van je wil weten en jij
      beslist of je die gegevens deelt. Je gegevens staan alleen op je mobiel,
      veilig achter een pincode. Niemand kijkt mee, ook Yivi niet. Dat is wel zo
      veilig.
    image: /img/vrouw-mobiel-laptop-Yivi-qr-code.webp
    imageAltText: Vrouw met mobiel en laptop
    visibility: true
  yivi_benefits:
    title: Voordelen met Yivi
    summary: >-
      Met Yivi, de ID app, pak jij de controle over jouw gegevens. En voorkom je
      een wildgroei aan accounts bij verschillende organisaties. Je beschermt je
      privacy door minder persoonlijke details te delen. Het zijn jouw gegevens,
      dus jij bent de baas.


      **All You. All yours.**
    benefits:
      - benefit:
          title: Jij beslist wat je deelt
          description: Je kiest zelf welke gegevens je deelt, met wie en wanneer.
      - benefit:
          title: Inloggen zonder wachtwoord
          description: Je hebt alleen je mobiele telefoon en pincode nodig om op allerlei
            websites in te loggen.
      - benefit:
          title: Veilig op jouw mobiel
          description: Je beheert zelf je persoonlijke gegevens op jouw mobiel. Alleen jij
            kan erbij, ook Yivi niet.
    visibility: true
  take_back_control:
    title: Pak de controle over je online gegevens
    image: /img/jongen-met-yivi-app-1.jpg
    imageAltText: Man laat Yivi-app zien
    content: >-
      Met Yivi kies jij bewust welke gegevens je deelt en met wie. Het zijn jouw
      gegevens, dus jij bepaalt. Ook online.


      **All you. All yours.**
    visibility: true
  signing_documents:
    subtitle: Zet je digitale handtekening in een handomdraai
    title: Ondertekenen
    content: Met Yivi kun je ook zaken digitaal ondertekenen en goedkeuren. Jouw
      persoonsgegevens vormen dan een soort digitale stempel. Zo kun je met Yivi
      digitaal ondertekenen als burger of iets digitaal goedkeuren vanuit een
      andere zakelijke rol.
    image: /img/digitale-ondertekening.webp
    imageAltText: digitaal ondertekenen
    visibility: true
  privacy_and_security:
    title: Jouw privacy en veiligheid staan centraal
    content: "Jij beslist wat en met wie je jouw gegevens deelt. Zo deel je nooit
      meer gegevens dan nodig. Wil je bijvoorbeeld tickets bestellen voor die
      spannende 18+film? Met Yivi bewijs je met een vinkje dat je 18 jaar of
      ouder bent, zonder je geboortedatum te delen. Zo bescherm je jouw privacy.
      Ook staan je gegevens overzichtelijk bij elkaar op één plek waar alleen
      jij bij kan: je mobiel. Niemand die met je meekijkt. Zelfs Yivi niet. Yivi
      is bovendien gebouwd door SIDN een organisatie zonder winstoogmerk."
    visibility: true
---
