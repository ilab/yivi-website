---
layout: yivi_ecosystem
title: Yivi ecosystem
sections:
  ecosystem_about:
    visibility: true
    title: The Yivi ecosystem
    content: What does the landscape of Yivi look like? We are happy to explain it
      to you. The ecosystem consists of three pillars. The data sources form the
      basis of the system and allow users to add reliable data to their wallet.
      The digital identity wallet resides on the user's mobile phone. The user
      adds their digital identity and personal data, giving them control and
      visibility over their data. Finally, you have service providers who need
      the data to serve the user.
    image: /img/qr-codescannen-voor-yivi.webp
    imageAltText: Scanning Yivi QR code
  ecosystem_illustrated:
    visibility: true
    title: The ecosystem illustrated
    image: /img/ecosysteem.webp
    imageAltText: Yivi ecosystem
  wallet:
    visibility: true
    title: Digital identity wallet
    content: >-
      Yivi is a digital wallet on your mobile phone, which you as a user fill
      with your digital identity and personal data. To do this, you log in to
      the government or other organisations, retrieve the data and add it to
      Yivi. In Yivi, you see an overview of all the data you have put in and the
      data you can still add.


      1. On the service provider's website (=verifier), the user clicks the Yivi button

      2. Under water, the website asks the Yivi server to exchange certain data

      3. A Yivi QR code appears on the website

      4. The user scans the QR code

      5. You see what data is requested and choose which data you want to share

      6. After obtaining permission, you share your data as a user

      7. If, as a user, you do not yet have all the required data, you first obtain these from the source(s), after which the correct data are automatically added to Yivi.


      With Yivi, you as a user have control over your data. The Yivi app records which data, with whom and for what purpose have been shared with your permission. Only you have access to this data on your mobile phone. No one else, including Yivi.


      Yivi also allows you as a user to log in to your online services securely without a password, similar to DigiD. You can also digitally approve or sign something. This works the same way. For this too, you scan a QR code and click OK to log in with your details or sign something.
    image: /img/ecosysteem_icon-2.webp
    imageAltText: 1. eID 2. Data sharing 3. Signing
  issuers:
    visibility: true
    title: Issuers
    subtitle: Data source(s)
    content: >-
      Due to various European Legislation such as GDPR, PSD2 and eIDAS, personal
      data is gradually becoming available to governments and organisations. As
      a user, you have the right to access, modify, collect and share this data
      with services you like to use.




      An issuer is an organisation that manages a data source with data and - after identification - unlocks this data to you as a user and issues it to your personal Yivi app. This can be both personal- and organisation-related data. For example, your name or address, but also a customer number or proof of which services you are allowed to use. This makes it easier and safer for you to use all kinds of services from various public and private service providers.




      An issuer ensures that:


      * The data are correct and issued to the right person.

      * The data source is always available, so that a person can always retrieve (or refresh) data.

      * Optional: if data is found to be no longer correct, the data is revoked (= revocation). This only applies when an issuer supports revocation.

      * Important: the issuer should carefully handle the "private key" that enables the issuance of data to Yivi.
    imageAltText: Database icon
    image: /img/database-icon.webp
  verifiers:
    visibility: true
    title: Verifiers
    subtitle: Data-using services
    content: >-
      A verifier is a service provider with which you as a user - after scanning
      a QR code - share your data from your Yivi app for e.g. identification,
      logging in, verifying data, filling in forms or digital signing. You share
      only the data needed for the service (= data minimisation and purpose
      limitation). Sometimes this is the data itself (e.g.: your date of birth),
      sometimes this is just a proof (e.g.: that you are over 18).


      The data in the user's Yivi app are digitally signed by the organisation that issued the data (= issuer). This allows the service provider to verify (cryptographically) which official organisation issued the data and thus guarantees the accuracy of the data.


      A service provider can specify with a data request: which data, from which organisation, with which maximum issue date are desired.


      With a verified digital identity and data, service providers can optimise their services, saving costs as well as developing new forms of service (e.g.: a sustainable mortgage based on your financial data and your energy data from your smart meter).
    image: /img/ecosysteem_icon-1.webp
    imageAltText: Three monitors with QR-code
---
