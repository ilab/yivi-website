---
layout: yivi_ecosystem
title: Yivi landschap
sections:
  ecosystem_about:
    visibility: true
    title: Het Yivi landschap
    content: Hoe ziet het landschap van Yivi eruit? We leggen het je graag uit. Het
      ecosysteem bestaat uit drie pijlers. De databronnen vormen de basis van
      het systeem en zorgen dat de gebruiker betrouwbare gegevens kan toevoegen
      aan zijn wallet. De digitale identity wallet staat op de mobiele telefoon
      van de gebruiker. De gebruiker voegt de digitale identiteit en
      persoonlijke gegevens toe en krijgt hierdoor zelf de controle en het
      overzicht over zijn of haar data. Tot slot heb je nog dienstverleners die
      de data nodig hebben om de gebruiker van dienst te kunnen zijn.
    image: /img/qr-codescannen-voor-yivi.webp
    imageAltText: Yivi QR code scannen
  ecosystem_illustrated:
    visibility: true
    title: Het Yivi landschap in beeld
    image: /img/ecosysteem.webp
    imageAltText: Yivi landschap
  wallet:
    visibility: true
    title: Digitale identiteit wallet
    content: >-
      Yivi is een digitale portemonnee op je mobiele telefoon, die je als
      gebruiker vult met je digitale identiteit en persoonlijke gegevens. Je
      logt hiervoor in bij de overheid of andere organisaties, haalt de gegevens
      op en voegt ze toe aan Yivi. In Yivi zie je een overzicht van alle
      gegevens die je erin hebt gezet en de gegevens die je nog kunt toevoegen.


      1. Op de website van de dienstverlener (=verifier) klik je als gebuiker op de Yivi-button

      2. Onder water vraagt de website aan de Yivi-server om bepaalde gegevens uit te wisselen

      3. Op de website verschijnt een Yivi QR-code

      4. Als gebruiker scan je de QR-code

      5. Je ziet welke gegevens gevraagd worden en kiest zelf welke gegevens jij deelt

      6. Na toestemming deel je als gebruiker je gegevens.

      7. Heb je als gebruiker nog niet alle benodigde gegevens, dan haal je deze eerst op bij de bron(nen) waarna automatisch de juiste gegevens aan Yivi toegevoegd worden.


      Met Yivi heb je als gebruiker de controle over je gegevens. De Yivi-app registreert welke gegevens, met wie en voor welk doel met jouw toestemming zijn gedeeld. Alleen jij hebt toegang tot deze gegevens op je mobiele telefoon. Niemand anders, ook Yivi niet.


      Met Yivi kun je als gebruiker ook inloggen bij je online diensten veilig zonder wachtwoord, vergelijkbaar met DigiD. Je kunt ook digitaal iets kan accorderen of ondertekenen. Dit werkt hetzelfde. Ook hiervoor scan je een QR-code en klik je op OK om met je gegevens in te loggen of iets te ondertekenen.
    image: /img/ecosysteem_icon-2.webp
    imageAltText: 1. eID 2. Data sharing 3. Signing
  issuers:
    visibility: true
    title: Issuers
    subtitle: Databron(nen)
    content: >-
      Door verschillende Europese Wetgeving zoals GDPR, PSD2 en eIDAS komen
      persoonlijke gegevens stapsgewijs beschikbaar bij de overheid en
      organisaties. Je hebt als gebruiker het recht om deze gegevens in te zien,
      te wijzigen, te verzamelen en te delen met diensten die je graag gebruikt.




      Een issuer is een organisatie die een databron met gegevens beheert en die - na identificatie - deze gegevens naar jou als gebruiker ontsluit en uitgeeft aan je persoonlijke Yivi app. Dit kunnen zowel persoonlijke- als organisatie gerelateerde gegevens zijn. Bijvoorbeeld je naam of adres, maar ook een klantnummer of een bewijs van welke dienstverlening je gebruik mag maken. Hierdoor kan je makkelijker en veiliger allerlei diensten van diverse publieke en private dienstverleners gebruiken.




      Een issuer zorgt ervoor dat:


      * De gegevens juist zijn en aan de juiste persoon worden uitgegeven.

      * De databron altijd beschikbaar is, zodat een persoon altijd gegevens kan ophalen (of verversen).

      * Optioneel: als gegevens niet meer juist blijken te zijn, de gegevens worden herroepen (= revocatie). Dit is alleen van toepassing wanneer een issuer revocatie ondersteunt.

      * Belangrijk: de issuer dient zorgvuldig om te gaan met de "private key" die het uitgeven van gegevens aan Yivi mogelijk maakt.
    image: /img/database-icon.webp
    imageAltText: Database icoon
  verifiers:
    visibility: true
    title: Verifiers
    subtitle: Data gebruikende diensten
    content: >-
      Een verifier is een dienstverlener waarmee je als gebruiker - na het
      scannen van een QR-code - je gegevens uit jouw Yivi-app deelt voor
      bijvoorbeeld: identificatie, inloggen, verifiëren van gegevens, invullen
      van formulieren of digitaal ondertekenen. Je deelt alleen de gegevens die
      nodig zijn voor de dienstverlening (= dataminimalisatie en doelbinding).
      Soms is dat het gegeven zelf (bv: je geboortedatum), soms is dit alleen
      een bewijs (bv: dat je ouder bent dan 18 jaar).


      De gegevens in de Yivi-app van de gebruiker zijn digitaal ondertekend door de organisatie die de gegevens heeft uitgegeven (= issuer). Hiermee kan de dienstverlener (cryptografisch) verifiëren welke officiële organisatie de gegevens heeft uitgegeven en daarmee garant staat voor de juistheid van de gegevens.


      Een dienstverlener kan bij een gegevensverzoek aangeven: welke gegevens, van welke organisatie, met welke maximale uitgiftedatum gewenst zijn.


      Met een geverifieerde digitale identiteit en gegevens kunnen dienstverleners hun diensten optimaliseren en zo zowel kosten besparen als nieuwe vormen van dienstverlening ontwikkelen (bv: een duurzame hypotheek op basis van je financiële gegevens en je energie gegevens van je slimme meter).
    image: /img/ecosysteem_icon-1.webp
    imageAltText: Drie monitoren met QR-code
---
