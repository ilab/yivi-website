---
layout: how_yivi_works
title: How Yivi works
sections:
  how_yivi_works:
    content: "An account for your municipality, for a digital service, and then
      there’s 30 more accounts with different companies. Keeping track of them?
      A nightmare. So of course you’d rather store everything in one place. Yivi
      allows you to do just that. It lets you save your personal data in the
      safest place possible: your phone. We don’t need anything in return. Not
      even your data. You and you alone decide what you share. And who you share
      it with."
    title: How Yivi works
    visibility: true
  setup:
    steps:
      - step:
          title: Step 1
          description: Download the Yivi app in one of the app stores.
          image: /img/setup_step1.gif
          imageAltText: "Step one: download the yivi app"
      - step:
          description: Choose a PIN and enter your email address.
          title: Step 2
          image: /img/setup_step2.png
          imageAltText: Creating PIN code
      - step:
          title: Step 3
          description: Fill the Yivi app with your personal data from various sources.
          image: /img/setup_step3.png
          imageAltText: Your data
    title: Set up
    content: "It only takes 5 minutes to start signing in with Yivi. Here’s how it
      works:"
    visibility: true
  add_your_data:
    title: Add your data to the Yivi app
    content: >-
      Yivi's possibilities are endless. Proving you're 18+? Logging into a
      website? Voting anonymously? Or sign a document? It's all possible. For
      this, put your details in your app, such as:


      * Personal details

      * Address details

      * Contact details: email address(es) and mobile phone number(s)

      * Financial details

      * Education details

      * Healthcare details
    image: /img/vrouw-mobiel-laptop-Yivi-qr-code.webp
    imageAltText: Loggin in with Yivi
    visibility: true
  everyday_uses:
    title: Yivi’s every day uses
    login_mobile:
      image: /img/yivi-illustrations-inloggen-met-mobiel-breed.webp
      imageAltText: Log in Yivi with mobile
      title: Login or sharing data with your mobile
      content: >-
        * On your mobile, open the website you wish to log in to

        * Choose there the Yivi button to log in with Yivi

        * The Yivi app opens automatically and asks for your permission to share your data

        * If some data are not yet in your app, you can retrieve them with Yivi

        * Check that you indeed agree to share these data with the website and then press 'ok'

        * After your consent, the Yivi app shares your data directly with the website

        * You are now logged and can proceed on the website.
    login_laptop:
      title: Login or sharing data with your laptop or PC
      image: /img/yivi-illustrations-inloggen-met-mobiel.webp
      imageAltText: Logging in on laptop with Yivi
      content: >-
        * Open the website and press the Yivi button

        * You will then see a QR code on the site

        * Open the Yivi app on your mobile; after pressing the QR button in your Yivi app, scan this code

        * Yivi shows which data the site wants to receive

        * If some data are not yet in your app, you can retrieve them with Yivi

        * Check that you indeed agree to share these data with the website and then press 'ok'

        * After your consent, the Yivi app shares your data directly with the website

        * You are now logged in and can proceed on the website.
    visibility: true
  yivi_benefits:
    title: Yivi' benefits
    summary: >-
      Take control of your own data with Yivi, you ID app. And prevent a
      proliferation of accounts at various organisations. You protect your
      privacy by sharing fewer personal details. You’ll store your data in a
      place only you can access: your phone. It’s your data, so you’re in
      charge.


      **All You. All yours.**
    benefits:
      - benefit:
          title: In control of your data
          description: You decide what data to share and with whom
      - benefit:
          title: Login without passwords
          description: You only need a mobile phone and PIN to sign into many different
            websites.
      - benefit:
          title: Safely stored on your phone
          description: You’re the one who manages your personal data on your phone. Only
            you can access it, not even Yivi.
    visibility: true
  take_back_control:
    title: Take control of your digital data
    content: >-
      With Yivi, you choose which data you share, and who you share it with.
      It’s your data, so you’re in charge. On- and offline. 




      **All you. All yours.**
    image: /img/jongen-met-yivi-app-1.jpg
    imageAltText: Man shows Yivi app
    visibility: true
---
