---
layout: how_yivi_works
title: Hoe Yivi werkt
sections:
  how_yivi_works:
    title: Hoe Yivi werkt
    content: "Een account bij de gemeente, bij een online service, en bij nog wel 30
      andere bedrijven. Het ergste van alles? Je raakt het overzicht kwijt. Want
      het liefst bewaar je alles op één plek. Dan kan je mooi een oogje in het
      zeil houden. Met Yivi kan jij je persoonlijke gegevens verzamelen op de
      meest veilige plek: jouw mobiel. Daar hoeven we niks voor terug. Ook jouw
      gegevens niet. Jij, en jij alleen bepaalt wat je deelt en met wie."
    visibility: true
  setup:
    steps:
      - step:
          image: /img/setup_step1.gif
          title: Stap 1
          description: Download de Yivi-app uit een van de appstores.
          imageAltText: "Stap 1: download de yivi app"
      - step:
          title: Stap 2
          description: Kies een pincode en voer je e-mailadres in.
          image: /img/setup_step2.png
          imageAltText: Pincode aanmaken
      - step:
          title: Stap 3
          description: Vul de Yivi app met je persoonlijke gegevens, uit verschillende
            bronnen.
          image: /img/setup_step3.png
          imageAltText: Jouw gegevens
    title: Aan de slag met installeren
    content: "Binnen 5 minuten kan je inloggen met Yivi. Zo werkt dat:"
    visibility: true
  add_your_data:
    title: Zet jouw gegevens in de Yivi-app
    content: >-
      De mogelijkheden van Yivi zijn oneindig. Bewijzen dat je 18+ bent?
      Inloggen op een website? Anoniem stemmen? Of een document ondertekenen?
      Kan allemaal. Plaats hiervoor je gegevens in jouw app, zoals:


      * Persoonsgegevens

      * Adresgegevens

      * Contactgegevens: e-mailadres(sen) en mobiele telefoonnummer(s)

      * Financiële gegevens

      * Onderwijsgegevens

      * Zorggegevens
    image: /img/vrouw-mobiel-laptop-Yivi-qr-code.webp
    imageAltText: Inloggen met Yivi
    visibility: true
  everyday_uses:
    title: Yivi in dagelijks gebruik
    login_mobile:
      image: /img/yivi-illustrations-inloggen-met-mobiel-breed.webp
      imageAltText: Inloggen met mobiel op Yivi
      title: Inloggen of gegevens delen via je mobiel
      content: >-
        * Open op je mobiel de website waarop je wilt inloggen

        * Kies daar de Yivi-knop om in te loggen met Yivi

        * De Yivi-app opent vanzelf en vraagt om je om toestemming voor het delen van jouw gegevens

        * Staan enkele gegevens nog niet in je app, dan haal je deze met Yivi op

        * Controleer dat je deze gegevens inderdaad wil delen met de website en druk dan op ‘ok’.

        * Na je toestemming deelt de Yivi app jouw gegevens rechtstreeks met de website.

        * Je bent nu ingelogd en kunt verder op de website.
    login_laptop:
      title: Inloggen of gegevens delen via je laptop of PC
      image: /img/yivi-illustrations-inloggen-met-mobiel.webp
      imageAltText: Inloggen op laptop met Yivi
      content: >-
        * Open de website en druk op de Yivi-knop

        * Je krijgt dan een QR-code te zien op de site

        * Open de Yivi-app op je mobiel, druk in de app op de QR-knop en scan deze code

        * Yivi laat zien welke gegevens de website wil ontvangen

        * Staan enkele gegevens nog niet in je app, dan haal je deze met Yivi op

        * Controleer dat je deze gegevens inderdaad wil delen met de website en druk dan op ‘ok’

        * Na je toestemming deelt de Yivi app jouw gegevens rechtstreeks met de website

        * Je bent nu ingelogd en kunt verder op de website.
    visibility: true
  yivi_benefits:
    title: Voordelen met Yivi
    summary: >-
      Met Yivi, de ID app, pak jij de controle over jouw gegevens. En voorkom je
      een wildgroei aan accounts bij verschillende organisaties. Je beschermt je
      privacy door minder persoonlijke details te delen. Het zijn jouw gegevens,
      dus jij bent de baas.


      **All You. All yours.**
    benefits:
      - benefit:
          title: Jij beslist wat je deelt
          description: Je kiest zelf welke gegevens je deelt, met wie en wanneer.
      - benefit:
          title: Inloggen zonder wachtwoord
          description: Je hebt alleen je mobiele telefoon en pincode nodig om op allerlei
            websites in te loggen.
      - benefit:
          title: Veilig op jouw mobiel
          description: Je beheert zelf je persoonlijke gegevens op jouw mobiel. Alleen jij
            kan erbij, ook Yivi niet.
    visibility: true
  take_back_control:
    title: Pak de controle over je online gegevens
    content: >-
      Met Yivi kies jij bewust welke gegevens je deelt en met wie. Het zijn jouw
      gegevens, dus jij bepaalt. Ook online.




      **All you. All yours.**
    image: /img/jongen-met-yivi-app-1.jpg
    imageAltText: Man laat Yivi app zien
    visibility: true
---
