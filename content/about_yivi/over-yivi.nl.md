---
layout: about_yivi
title: Over Yivi
sections:
  about:
    visibility: true
    title: Over Yivi
    content: >-
      Yivi staat voor 'Jouw digitale leven' en spreek je uit als Yivi.


      Achter Yivi zitten twee organisaties, die nauw samenwerken aan de adoptie van Yivi. De [Stichting Privacy by Design](https://privacybydesign.foundation/) is [](<>)de eigenaar is van de Yivi merkrechten. Het bedrijf [Caesar Group](https://caesar.nl/yivi/) [](<>)[](<>)is verantwoordelijk voor de verdere ontwikkeling en het beheer van Yivi. Bij deze samenwerking zijn het privacy-vriendelijke karakter van de Yivi app, de openheid van de source, gratis beschikbaarheid voor eindgebruikers en goede en beveiligde werking (contractueel) gegarandeerd.
    image: /img/yivi_logo.webp
    imageAltText: Yivi logo
  development:
    visibility: true
    title: De ontwikkeling van Yivi
    content: De Yivi-app is de opvolger van de IRMA-app, de eerste
      privacy-vriendelijke digitale identiteit wallet, gebaseerd op persoonlijke
      attributen. IRMA (hernoemd tot  Yivi) is ontwikkeld door [Bart
      Jacobs](https://www.cs.ru.nl/~bart/) met zijn team aan de Radboud
      Universiteit in Nijmegen. Jacobs is ook voorzitter van de [Stichting
      Privacy by Design](https://privacybydesign.foundation/). In 2019 startte
      deze stichting een samenwerking met [SIDN](https://www.sidn.nl/), waarbij
      SIDN verantwoordelijk werd voor het beheer van de backbone van Yivi,
      waarmee de technologie, continuïteit en beschikbaarheid voor jaren
      garandeerd werd. SIDN zorgde voor het uitbouwen van het merk en het
      doorontwikkelen van de app en de backbone. Vanaf het najaar van 2024 neemt
      het Utrechtse bedrijf [Caesar Group](https://caesar.nl/yivi/)
      d[](https://caesar.nl/yivi/)[](https://caesar.nl/)e verdere ontwikkeling
      en het beheer van Yivi over van SIDN, in nauwe samenwerking met de
      Stichting Privacy by Design.
    image: /img/banner_bart_jacobs_and_roelof_meijer_with_irma_logo.webp
    imageAltText: Banner Bart Jacobs en Roelof Meijer met IRMA logo
  mission:
    visibility: true
    title: Onze missie
    content: Internetgebruikers wordt te pas en te onpas gevraagd om
      privacygevoelige gegevens te delen en vaak delen ze meer informatie dan
      nodig is. Dat kan en moet anders. Wij geloven dat de ontwikkeling van
      eenvoudige, privacyvriendelijke en veilig te gebruiken oplossingen voor
      online identificatie bijdraagt aan de bescherming van privacy en
      identiteit en een optimaal gebruik van het internet. En daarmee dus aan
      onze missie voor een kansrijk en zorgeloos digitaal bestaan voor iedereen.
---
