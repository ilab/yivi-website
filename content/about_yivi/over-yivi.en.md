---
layout: about_yivi
title: About Yivi
sections:
  about:
    visibility: true
    image: /img/yivi_logo.webp
    title: About Yivi
    content: >-
      Yivi stands for 'Your digital life' and you pronounce it as Yivi.


      There are two organisations behind Yivi that closely collaborate for Yivi adoption:. The [Privacy by Design Foundation](https://privacybydesign.foundation/en/) o[](<>)wns the Yivi brand rights. The Dutch company [Caesar Group](https://caesar.nl/yivi/) [](<>)is responsible for the further development and management of Yivi. The cooperation guarantees, by contract,  the privacy-friendly character of the Yivi app, the opnness of its source code, the free use for end-users, and the proper, secure working.
    imageAltText: Yivi logo
  development:
    visibility: true
    title: The development of Yivi
    content: The Yivi app is the successor to the IRMA app, the first
      privacy-friendly digital wallet based on personal attributes. IRMA
      (renamed to Yivi) was developed by [Bart
      Jacobs](https://www.cs.ru.nl/~bart/) with his team at Radboud University
      in Nijmegen. Jacobs is also chairman of the [Privacy by Design
      Foundation](https://privacybydesign.foundation/en/). In 2019,
      collaboration with [SIDN](https://www.sidn.nl/en) began, with SIDN taking
      over responsibility for managing IRMA's backbone, to ensure the
      technology's essential continuity and availability. Since the fall of 2024
      the Dutch company [Caesar Group](https://caesar.nl/yivi/)
      [](https://caesar.nl/yivi/)takes over from SIDN the further development
      and management of Yivi, in close cooperation with the Privacy by Design
      Foundation.
    imageAltText: Banner Bart Jacobs and Roelof Meijer with IRMA logo
    image: /img/banner_bart_jacobs_and_roelof_meijer_with_irma_logo.webp
  mission:
    visibility: true
    title: Our mission statement
    content: Internet users are asked to share privacy-sensitive data too often and
      often share more information than necessary. This can and should be done
      differently. We believe that developing simple, privacy-friendly and
      secure solutions for online identification contributes to the protection
      of privacy and identity and optimal use of the Internet. And thus
      contribute to our mission for a promising and carefree digital existence
      for everyone.
---
