---
layout: privacy
title: Privacy
text: Your privacy is very important to us. We are therefore careful to make
  sure we abide by all privacy protection laws. So your data is safe with us and
  always treated with respect. This privacy statement explains what we do with
  the information that we receive about you. If you have any questions or want
  to know exactly what information we hold about you, please get in touch.
sections:
  - condition:
      title: Privacy Statement
      content: >-
        **Version 1 march 2023**




        SIDN Business B.V. (referred to below as 'SIDN' and 'we'/'us') processes personal data in order to provide attribute-based authentication and signing services by means of the Yivi system. We are responsible for that processing, and we attach great importance to your privacy. We are therefore careful to make sure we abide by all privacy protection laws. Your data is therefore safe with us and always treated with respect.




        Yivi is designed so that personal data, in the form of 'attributes', is stored in encrypted form exclusively on the phone or tablet where you install the Yivi app. Another feature of Yivi is that you decide who your personal data is shared with.


        An attribute is a small package of data that typically states a characteristic of its owner (e.g. 'over 18').Examples include your bank account number, name, postal address, e-mail address, mobile number or Public Service Number (BSN).No one else, not even SIDN, can see which issuers you get attributes from, or who you share them with. Nor can anyone else see what attributes you obtain or share. What's more, attributes can't be viewed or shared except using the Yivi app. The whole system is designed for privacy.




        Yivi's purpose is to enable secure, confidential, data-minimised logins, service access, consent and signing involving third-party authentication via the Yivi system.
    section:
      title: Privacy Statement
      content: >-
        **Version 1 november 2024**


        The Privacy by Design Foundation, Caesar Group and SIDN Business B.V. (referred to below as 'operators') processes personal data in order to provide attribute-based authentication and signing services by means of the Yivi system. They are responsible for that processing, and they attach great importance to your privacy. They are therefore careful to make sure they abide by all privacy protection laws. Your data is therefore safe with them and always treated with respect.


        Yivi is designed so that personal data, in the form of 'attributes', is stored in encrypted form exclusively on the phone or tablet where you install the Yivi app. Another feature of Yivi is that you decide who your personal data is shared with.


        An attribute is a small package of data that typically states a characteristic of its owner (e.g. 'over 18').Examples include your bank account number, name, postal address, e-mail address, mobile number or Public Service Number (BSN).No one else, not even the operators, can see which issuers you get attributes from, or who you share them with. Nor can anyone else see what attributes you obtain or share. What's more, attributes can't be viewed or shared except using the Yivi app. The whole system is designed for privacy.


        Yivi's purpose is to enable secure, confidential, data-minimised logins, service access, consent and signing involving third-party authentication via the Yivi system.
  - condition:
      title: What do we do with your data?
      content: >-
        What we do with the data we receive from you when you use Yivi is
        explained below.




        When you create a Yivi account, we record only a random username (app ID) and, if you choose to give one, an e-mail address. We also save a small amount of historical usage data, as described below.
    section:
      content: >-
        What the operators do with the data that they receive from you when you
        use Yivi is explained below.


        When you create a Yivi account, the operators record only a random username (app ID) and, if you choose to give one, an e-mail address. They also save a small amount of historical usage data, as described below.
      title: What do the operators do with your data?
  - condition:
      title: Legitimate basis
      content: >-
        When acting as a data controller, we have legitimate grounds for
        processing personal data in connection with Yivi: we have your consent,
        and processing is necessary for fulfilment of our agreement with you. We
        need to process personal data in order to assure the functionality of
        the Yivi system and app. Your mobile number and credential e-mail
        address, the app ID and the historical usage data are needed for
        verification and for the app to work properly. Processing is therefore
        necessary for us to fulfil our agreement with you, the Yivi app user.




        If you choose to use the e-mail address-based recovery function, you explicitly consent to processing of the e-mail address you have given us.




        Also, every time you obtain or share an attribute or attributes, the Yivi app asks for your explicit consent. Your consent constitutes legitimate grounds for the processing of attributes by the third-party provider or recipient.
    section:
      title: Legitimate basis
      content: >-
        When acting as a data controller, the operators have legitimate grounds
        for processing personal data in connection with Yivi: they have your
        consent, and processing is necessary for fulfillment of their agreement
        with you. They need to process personal data in order to assure the
        functionality of the Yivi system and app. Your mobile number and e-mail
        address attribute, the app ID and the historical usage data are needed
        for verification and for the app to work properly. Processing is
        therefore necessary for them to fulfill their agreement with you, the
        Yivi app user.


        If you choose to use the e-mail address-based recovery function, you explicitly consent to processing of the e-mail address you have given to the operators.


        Also, every time you obtain or share an attribute or attributes, the Yivi app asks for your explicit consent. Your consent constitutes legitimate grounds for the processing of attributes by the third-party provider or recipient.
---
