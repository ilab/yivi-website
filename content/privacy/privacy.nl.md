---
layout: privacy
title: Privacy
text: Jouw privacy vinden wij erg belangrijk. Wij houden ons dan ook aan de
  privacywetgeving. Dit betekent dat je gegevens veilig zijn bij ons en dat we
  er netjes mee omgaan. In deze privacyverklaring leggen we uit wat we bij Yivi
  allemaal doen met informatie die we over jou te weten komen. Als je vragen
  hebt, of wilt weten wat we precies van jou bijhouden, neem dan contact met ons
  op.
sections:
  - condition:
      title: Yivi Privacyverklaring
      content: >-
        **Versie 1 maart 2023**




        SIDN Business B.V. (hierna: SIDN) verwerkt persoonsgegevens voor het realiseren van attribuut-gebaseerde authenticatie en ondertekening via het systeem Yivi. SIDN is daarbij verantwoordelijk voor de verwerking en vindt jouw privacy erg belangrijk. Wij houden ons dan ook aan de privacywetgeving. Dit betekent dat je gegevens veilig zijn bij ons en dat we er netjes mee omgaan.




        De opzet van Yivi is zodanig dat persoonsgegevens, in de vorm van attributen, exclusief bij de gebruiker zelf in de Yivi-app op de eigen telefoon of tablet versleuteld worden opgeslagen en dat de gebruiker zelf de controle heeft over met wie hij zijn persoonsgegevens deelt.




        Attributen zijn een klein stukje data dat doorgaans een verklaring bevat over de eigenaar van het attribuut (bijv. ‘> 18 jaar’). Een attribuut is bijvoorbeeld je banknummer, naam, huisadres, e-mailadres, 06-nummer of Burgerservicenummer (BSN). Niemand kan, ook SIDN niet, zien bij welke uitgever van attributen de gebruiker attributen ophaalt of met wie de gebruiker zijn attributen deelt. Welke attributen door de gebruiker worden opgehaald of gedeeld kan eveneens niemand zien. Opgehaalde attributen kunnen alleen via de Yivi-app geraadpleegd en gedeeld worden. Het hele stelsel is op privacy by design gebaseerd.




        Het doel van Yivi is om veilig en vertrouwelijk, op basis van dataminimalisatie te kunnen inloggen, toegang te kunnen verschaffen, toe te stemmen en te ondertekenen bij derde partijen die authenticatieprocessen via Yivi uitvoeren.
    section:
      title: Yivi Privacyverklaring
      content: >-
        **Versie 1 november 2024**


        De Stichting Privacy by Design, Caeser Groep en SIDN Business B.V. (hierna: de  beheerder) verwerken persoonsgegevens voor het realiseren van attribuut-gebaseerde authenticatie en ondertekening via het systeem Yivi. De beheerders zijn daarbij verantwoordelijk voor de verwerking en vindt jouw privacy erg belangrijk. Zij houden zich dan ook aan de privacywetgeving. Dit betekent dat je gegevens veilig zijn bij hen en dat zij er netjes mee omgaan.


        De opzet van Yivi is zodanig dat persoonsgegevens, in de vorm van attributen, exclusief bij de gebruiker zelf in de Yivi-app op de eigen telefoon of tablet versleuteld worden opgeslagen en dat de gebruiker zelf de controle heeft over met wie hij zijn persoonsgegevens deelt.


        Attributen zijn een klein stukje data dat doorgaans een verklaring bevat over de eigenaar van het attribuut (bijv. ‘> 18 jaar’). Een attribuut is bijvoorbeeld je bankrekeningnummer, naam, huisadres, e-mailadres, 06-nummer of Burgerservicenummer (BSN). Niemand kan, ook beheerders niet, zien bij welke uitgever van attributen de gebruiker attributen ophaalt of met wie de gebruiker zijn attributen deelt. Welke attributen door de gebruiker worden opgehaald of gedeeld kan eveneens niemand zien. Opgehaalde attributen kunnen alleen via de Yivi-app geraadpleegd en gedeeld worden. Het hele stelsel is op privacy by design gebaseerd.


        Het doel van Yivi is om veilig en vertrouwelijk, op basis van dataminimalisatie te kunnen inloggen, toegang te kunnen verschaffen, toe te stemmen en te ondertekenen bij derde partijen die authenticatieprocessen via Yivi uitvoeren.
  - condition:
      title: Wat doen we met jouw gegevens?
      content: >-
        Hieronder lees je wat wij doen met de gegevens die wij van jou
        verzamelen via Yivi.




        Bij de registratie van nieuwe Yivi-gebruikers bewaart SIDN per gebruiker enkel een willekeurig gebruikersnaam (het app-ID) en, alleen als de gebruiker deze zelf opgeeft, een e-mailadres. Daarnaast slaan we een zeer beperkte verzameling historische gebruiksgegevens op, zoals hieronder nader beschreven wordt.
    section:
      title: Wat doen de beheerders met jouw gegevens?
      content: >-
        Hieronder lees je wat de beheerders doen met de gegevens die zij van jou
        verzamelen via Yivi.


        Bij de registratie van nieuwe Yivi-gebruikers bewaren de beheerders per gebruiker enkel een willekeurig gebruikersnaam (het app-ID) en, alleen als de gebruiker deze zelf opgeeft, een e-mailadres. Daarnaast slaan ze een zeer beperkte verzameling historische gebruiksgegevens op, zoals hieronder nader beschreven wordt.
  - condition:
      title: Grondslag
      content: >-
        De wettelijke grondslagen voor de verwerkingen door SIDN als
        verwerkingsverantwoordelijke, in het kader van Yivi, zijn toestemming
        van de betrokkene en noodzakelijk voor het uitvoeren van de
        gebruikersovereenkomst. De verwerking van persoonsgegevens door SIDN is
        noodzakelijk om de functionaliteiten van Yivi en de werking van de app
        te kunnen waarborgen. Zo is het verwerken van het mobiele nummer, het
        e-mailadres als credential, het app-ID en de historische
        gebruiksgegevens noodzakelijk voor de verificatie en werking van de app.
        Daarmee is de verwerking noodzakelijk ter uitvoering van de overeenkomst
        tussen SIDN en de gebruiker van de Yivi-app.




        Daarnaast geeft de gebruiker, wanneer hij ervoor kiest om van de herstelfunctie gebruik te maken, via het doorgeven het e-mailadres, expliciet toestemming om het herstel e-mailadres te laten verwerken.




        De Yivi-app vraagt de gebruiker bij iedere ontvangst en bij iedere onthulling van attributen overigens ook expliciet om toestemming voor deze actie. Dit vormt de grondslag voor de verwerking van de betreffende attributen door deze (derde) partijen.
    section:
      title: Grondslag
      content: >-
        De wettelijke grondslagen voor de verwerkingen door beheerders als
        verwerkingsverantwoordelijken, in het kader van Yivi, zijn toestemming
        van de betrokkene en noodzakelijk voor het uitvoeren van de
        gebruikersovereenkomst. De verwerking van persoonsgegevens door de
        beheerders is noodzakelijk om de functionaliteiten van Yivi en de
        werking van de app te kunnen waarborgen. Zo is het verwerken van het
        mobiele nummer, het e-mailadres als attribuut, het app-ID en de
        historische gebruiksgegevens noodzakelijk voor de verificatie en werking
        van de app. Daarmee is de verwerking noodzakelijk ter uitvoering van de
        overeenkomst tussen de beheerders en de gebruiker van de Yivi-app.


        Daarnaast geeft de gebruiker, wanneer hij/zij ervoor kiest om van de herstelfunctie gebruik te maken, via het doorgeven het e-mailadres, expliciet toestemming aan de beheerders om het herstel e-mailadres te laten verwerken.


        De Yivi-app vraagt de gebruiker bij iedere ontvangst en bij iedere onthulling van attributen overigens ook expliciet om toestemming voor deze actie. Dit vormt de grondslag voor de verwerking van de betreffende attributen door deze (derde) partijen.
---
