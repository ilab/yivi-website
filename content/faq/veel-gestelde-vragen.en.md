---
layout: faq
title: Frequently asked questions
list_of_QA:
  - qa:
      question: What is Yivi?
      answer: Yivi is an app that lets you easily and securely log in, share data and
        prove who you are. Without sharing too much about yourself. With Yivi,
        you take control of your data. You always see what an organisation wants
        to know about you and you decide whether to share that data. Your data
        is only on your mobile, safely behind a PIN code. No one is watching,
        not even Yivi. That is how safe it is.
  - qa:
      answer: >-
        You’ll find a Yivi button on websites that offer Yivi. It’ll probably
        say something like Log in with Yivi. Don’t have Yivi yet? Download the
        app and enter your details. Use Yivi to sign in on your phone, computer,
        or laptop:




        * **On your phone?** Tap the Yivi login button on the website you’re visiting. This’ll immediately open the Yivi app. Enter your PIN and decide what data to share. Hit ‘OK’ to confirm. You’ll automatically return to the website and be signed in.

        * **On your computer or laptop?** Click on the Yivi login button on the website you’re visiting. You’ll see a QR code. Open the Yivi app on your phone by entering your PIN. Scan the QR code with the app. Decide what data to share and hit ‘OK’ to confirm. You’ll return to the website and be signed in.




        Is a website asking for data that’s not stored in your Yivi app yet? You can easily add new data in ‘My data’.
      question: How do I use Yivi to sign into a website?
  - qa:
      question: What can I store in the Yivi app?
      answer: >-
        Whatever information you want! Your email address(es), and phone
        numbers.. Your name, address, date of birth, or citizen service number.
        Whether you’re over 16, 18, or 65. Your student number or educator’s
        registrations. Or, if you work in healthcare, your AGB data.




        How? Collect your personal data using DigiD, or from your Dutch municipality. By doing so, you can be sure your data won’t contain any errors or typos. Then add it to Yivi – easy. Connect your data to your municipality using DigiD, which’ll only take a minute. Afterwards, the app will automatically enter all sorts of data on your behalf. You could also connect your data to your social media accounts. That’s usually enough to prove you’re you. We’re growing fast. More and more organisations are joining us.
  - qa:
      question: Where is my data stored?
      answer: It’s all in the Yivi app – nowhere else. There’s no central storage like
        a cloud. This way, you’re the only one with access to your data. And any
        data you delete is gone for good. So you’re in complete control. Only
        you decide what you share. And who you share it with.
  - qa:
      question: What happens to my data?
      answer: When you sign into a website, your phone contacts the website. After you
        give permission, the Yivi app shares whichever data you’ve approved for
        sharing. No intermediary agents. That’s why nobody at Yivi can see your
        data. And we don’t know what you share with websites either. Total
        privacy. Always.
  - qa:
      question: What makes Yivi so secure and privacy friendly?
      answer: Have people take back control of their digital data. That’s our mission
        at Yivi. What you save and share, and who you share it with is
        completely up to you. Just use the Yivi app on your phone to decide what
        people online are allowed to know about you. Without anyone watching –
        not even us. All your data is safely stored in the app, which you can’t
        open without a PIN. There’s no central storage or cloud. But what if you
        lose your phone? No need to panic. Simply remotely block your Yivi app
        and nobody will be able to steal your stuff. The data you collect (from
        the municipality or the government, for example) and store in the Yivi
        app has been digitally verified. It’s how a website or shop can tell
        your data is real. And that it doesn’t need further verification by any
        additional third parties.
  - qa:
      question: Who are the minds behind Yivi?
      answer: The Privacy by Design Foundation owns the rights of the brand Yivi, of
        its logo, and of several associated websites such as yivi.app and
        yivi.nl. The foundation cooperates with the Dutch company Caesar Group
        for the development and deployment of Yivi. In this cooperation the open
        source character and privacy-friendliness of Yivi are guaranteed.
  - qa:
      question: I want to subscribe from one of the organisations I log into with
        Yivi. Is that possible?
      answer: Unfortunately, we cannot cancel you. The quickest way to do this is to
        contact the organisation!
  - qa:
      question: How is Yivi free?
      answer: No one should make money off of your data – ever. That’s why Yivi is
        completely free. OK, so what’s the catch? There is none. Really. The
        only ones who pay are Yivi providers, so they can connect to the
        service. It’s how we’ll always be able to offer Yivi to you for free.
  - qa:
      question: Yivi is open source. But what does that mean?
      answer: "Yivi’s software is open source. That means that anyone can see how Yivi
        works. Of course, that doesn’t mean anyone can see your data. That’s
        only for you to access. By being open source, people outside of our
        organisation can help make Yivi even better. And, most importantly: the
        number one reason for open source is transparency. We don’t have any
        secrets, and technical experts can review our work."
  - qa:
      question: Why is data only valid for a limited time and how do I refresh it?
      answer: The entity where you collect the data from determines the period it’s
        valid for. Which means it can expire. When that happens, you’ll get a
        notification in the Yivi app. The validity period of your data depends
        on the kind of data. For example, a name and date of birth have a 5-year
        validity period. Your address is only valid for 1 year. You can refresh
        your data whenever you want. Say you turn 18. Refresh your age in the
        app and you can prove you’re over 18 straightaway.
  - qa:
      question: Where can I find the QR code on an organization's website?
      answer: There’s usually a button that says something like ‘sign in’ or ‘share
        data’. In some cases, an organisation doesn’t support Yivi (yet). Get in
        touch with them and they’ll help you further.
  - qa:
      question: How di I scan Yivi's QR code?
      answer: First, open your Yivi app. Hit ‘Scan QR’ at the bottom of the screen.
        Point your phone camera to the QR code you want to scan. Sometimes, your
        phone will ask for your permission to use your camera. Do so to start
        scanning.
  - qa:
      question: Can I use Yivi abroad?
      answer: You bet! You can use Yivi anywhere in the world. In fact, more and more
        websites from outside the Netherlands are joining us.
  - qa:
      question: What does 'security level' mean when it comes to personal data?
      answer: Using DigiD to add data to Yivi? Your security level is automatically
        set to ‘Medium’. Want to raise it? Link your passport or ID to your
        DigiD app. Then use Yivi to collect your data again. Want to know more?
        Head to [www.digid.nl](https://www.digid.nl/).
  - qa:
      question: How do I change my address or other data?
      answer: The organisation you collect the data from determines its contents. You
        should always let your municipality know if your personal details
        change. They’ll update them. Afterwards, you can collect your new data
        from them again and store them in Yivi.
  - qa:
      question: The Yivi button on a website doesn't open the link to the app. What do
        I do?
      answer: That’s annoying. But it’s often easy to fix. Using Firefox on an Android
        phone? Change your settings – otherwise Firefox won’t open links from
        third-party apps. Open ‘Settings’. Scroll down to the ‘Advanced’
        section. Then the slider button next to ‘Open links in apps’ to turn
        this on. If you’re using a different browser or a different phone, the
        link might be broken. Which means there’s nothing you can do. Well,
        except contact the website’s administrator. Still no luck? We might be
        able to help you out. Email us at
        [support@yivi.app](mailto:support@yivi.app).
  - qa:
      question: What are the minimum requirements for my mobile device to use the Yivi
        app?
      answer: >-
        To use Yivi, for an:




        * **iPhone** require at least the operating system version iOS 12.0. This is available for iPhone 5s and above.

        * **Android** at least need the Android 6 operating system and the device must have a processor with the ARM, ARM64 or x86-64 architecture.
  - qa:
      question: Can I install Yivi on another phone?
      answer: Definitely! In fact, it’s recommended. Download the app and enter your
        data. Just like you did on your ‘first’ phone. You can now use Yivi on
        your secondary phone should you lose your primary phone.
  - qa:
      question: I have a new phone. What now?
      answer: Simply install the Yivi app on your new phone. Afterwards, collect your
        data again. It’ll only take a second. Unfortunately, it’s not possible
        to transfer data from one Yivi app to the other at this time.
  - qa:
      question: I lost my phone! What do I do?
      answer: I'm sorry that happened! If you lose your mobile phone, no one can
        access your Yivi app. Because you have secured it well with your PIN
        code. Have you provided your email address via My Yivi \[My Yivi link]?
        Then you can block the app remotely. This way your data is 100% safe.
        Have you already installed Yivi on another device? Then you can continue
        to log in to sites that support Yivi.
  - qa:
      question: I want to delete Yivi and all my data. How does that work?
      answer: You can delete all your data in My Yivi. Then delete the Yivi app from
        your phone. If it’s been a year since you last used the app, we’ll
        automatically delete all the data it stores on your phone. Don’t worry,
        we’ll give you a head’s up before we do.
  - qa:
      question: I have a question. Who do I turn to?
      answer: Have a question about using Yivi to sign into a shop or organisation?
        It’s best to reach out to them directly. Have a different question? Or
        do you need help? Drop us a line at
        [support@yivi.app](mailto:support@yivi.app). Let us know what issues
        you’re having and what kind of phone you’re using, so we can help you as
        quickly as possible.
  - qa:
      question: What's the difference between Yivi and other ID solutions like DigiD,
        iDIN, or Facebook?
      answer: The biggest difference between Yivi and other providers is our
        decentralised structure. Apologies for the jargon – what we’re saying is
        that with Yivi, data is only stored on your phone. No other centralised
        storage system. Other organisations do make use of these storage
        systems. This allows them to collect lots of information about you
        (often without your explicit consent). Facebook even uses your data to
        create a profile so they can send you targeted ads. With Yivi, no one is
        watching. Not even the organisation behind the Yivi app. After all,
        you’re in charge of your data, and nobody else.
  - qa:
      question: How Yivi works?
      answer: >-
        Up and running with 3 simple tools:




        1. **Yivi button on your website** The customer hits the Yivi button to use your service within the Yivi app.

        2. **Yivi server application** The customer hits the Yivi button. The server, the link between your website and the user, will create a session. Install the application on your own server. Or use the server of your software provider.

        3. **Mobile Yivi app** The identity wallet on your customer’s phone. With it, they can easily, quickly, and securely make use of your services.
---
