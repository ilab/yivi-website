---
layout: faq
title: Veel gestelde vragen
list_of_QA:
  - qa:
      question: Wat is Yivi?
      answer: Yivi is een app waarmee je makkelijk en veilig inlogt, gegevens deelt en
        bewijst wie je bent. Zonder dat je te veel over jezelf deelt. Met Yivi
        pak jij de controle over jouw gegevens. Je ziet altijd wat een
        organisatie van je wil weten en jij beslist of je die gegevens deelt. Je
        gegevens staan alleen op je mobiel, veilig achter een pincode. Niemand
        kijkt mee, ook Yivi niet. Dat is wel zo veilig.
  - qa:
      question: Hoe log ik met Yivi in op een website?
      answer: >-
        Op websites die Yivi aanbieden vind je een speciale Yivi-knop.
        Bijvoorbeeld met de tekst ‘Log in met Yivi’. Ga van start door de
        Yivi-app te installeren en jouw gegevens in te vullen. Log in met je
        mobiel, je computer of je laptop:




        * **Zit je op je mobiel?** Klik dan op de Yivi-inlogknop als je op een website bent. We sturen je direct door naar de app. Vul je pincode in en kies hier bewust welke gegevens je deelt. Bevestig dan met ‘ok’. Je keert terug en bent direct ingelogd.

        * **Zit je op je computer of laptop?** Klik dan op de Yivi-inlogknop als je op een website bent. Daar zie je een QR-code. Open de Yivi-app op je mobiel met je pincode. Scan de QR-code. Kies dan bewust welke gegevens je deelt. Bevestig met ‘ok’. Je keert terug en bent direct ingelogd.




        Vraagt een website om gegevens te delen die nog niet in je Yivi-app staan? Dan kun je deze eenvoudig zelf toevoegen via ‘Mijn gegevens’.
  - qa:
      question: Welke gegevens kan ik in de Yivi-app zetten?
      answer: "Je haalt je persoonsgegevens op bij de gemeente of DigiD. Deze voeg je
        eenvoudig toe in Yivi. Zo zijn alle gegevens gegarandeerd  vrij van
        fouten. Gegevens die in Yivi staan zijn onder andere: je  naam, adres,
        geboortedatum of BSN. Maar ook of je 16, 18 of 65+  bent. Extra handig:
        je kan verschillende e-mailadressen of  telefoonnummers toevoegen. Ben
        je student of medewerker in het  (hoger) onderwijs? Je voegt je
        registratie in een handomdraai toe.  Ook zorgmedewerkers kunnen hun
        AGB-registraties toevoegen.  Koppel je gegevens aan je DigiD. Dat is zo
        gedaan. Daarna vult de  app automatisch allerlei gegevens in. Een andere
        optie is het  koppelen aan je social media. Vaak is deze info al
        voldoende om  jezelf bekend te maken. We groeien hard. En steeds
        meer  organisaties sluiten zich bij ons aan."
  - qa:
      question: Waar staan mijn gegevens opgeslagen?
      answer: Alle gegevens die je in de Yivi app zet, staan alleen daar. Er is geen
        centraal opslagpunt zoals een cloud. Zo ben jij de enige die bij je
        eigen gegevens kan. Verwijder je gegevens uit de app? Dan zijn ze ook
        echt weg. Jij bent volledig in controle over je gegevens. Alleen jij
        bepaalt wat je deelt. En met wie.
  - qa:
      question: Wat gebeurt er met mijn gegevens?
      answer: Als je inlogt bij een website maakt je telefoon contact met deze site.
        Nadat jij toestemming geeft, deelt de app de gegevens waar jij
        toestemming voor gegeven hebt. Zonder tussenpersoon. Daarom kan niemand
        bij Yivi jouw gegevens zien. En weten we ook niet wat je deelt met een
        website. 100% privacy dus.
  - qa:
      question: Wat maakt Yivi zo veilig en privacyvriendelijk?
      answer: Mensen de controle laten terugpakken over hun online gegevens. Dat is de
        missie van Yivi. Wat sla je op? Wat deel je? En met wie? Yivi laat de
        keuze aan jou. Met jouw mobiel bepaal je in een handomdraai wat mensen
        online van je mogen weten. Niemand kijkt mee. Ook Yivi niet. Alles staat
        veilig in je eigen app, met pincode. Er is geen centrale opslagplek of
        cloud. Telefoon verloren? Geen paniek. Blokkeer jouw Yivi-app op
        afstand. Zo gaat niemand met je gegevens aan de haal. De gegevens die je
        ophaalt (bijvoorbeeld bij de gemeente of overheid) en in de Yivi-app
        zet, zijn digitaal ondertekend door hen. Daarom weet een website of
        winkel direct dat jouw gegevens echt zijn. En hoeven deze niet
        gecontroleerd te worden door anderen.
  - qa:
      question: Wie zit er achter Yivi?
      answer: De Stichting Privacy by Design is eigenaar van het merk Yivi, van het
        logo, en van verschillende bijbehorende websites, zoals yivi.app en
        yivi.nl. De stichting werkt samen met het Nederlandse bedrijf Caesar
        groep voor de verdere ontwikkeling en het beheer van Yivi.  Bij die
        samenwerking is het open source en privacy-vriendelijke karakter van
        Yivi gegarandeerd.
  - qa:
      question: Ik wil mij afmelden bij een van de organisaties waarop ik inlog met
        Yivi. Kan dat?
      answer: Wij kunnen je helaas niet afmelden. Je doet dit het snelst door contact
        op te nemen met de organisatie!
  - qa:
      question: Hoe kan Yivi gratis zijn?
      answer: Jouw gegevens zijn géén verdienmodel! Daarom is Yivi helemaal gratis.
        Waar het addertje dan zit? Die is er niet, echt waar! Alleen de
        aanbieders van Yivi betalen. Zodat zij aangesloten zijn. Zo blijft Yivi
        voor jou altijd 100% gratis.
  - qa:
      question: Yivi is open source. Wat betekent dat eigenlijk?
      answer: "De software van Yivi is open source. Dat betekent dat de manier waarop
        Yivi werkt door iedereen bekeken kan worden. Natuurlijk gaat dit niet
        over je gegevens. Die zijn namelijk alleen door jou te zien. Door open
        source te zijn, kunnen mensen buiten de stichting ook helpen Yivi beter
        te maken. En niet te vergeten: de allerbelangrijkste reden voor open
        source is 100% transparantie. We hebben geen geheimen, en experts met
        technische kennis kunnen onze manier van werken controleren."
  - qa:
      question: Waarom zijn gegevens beperkt geldig en hoe ververs ik ze?
      answer: Degene bij wie je de gegevens ophaalt, bepaalt de geldigheidsduur. Deze
        kan dus verlopen. Je krijgt dan een waarschuwing in de Yivi-app. Hoe
        lang je gegevens geldig zijn? Dat ligt aan het type gegevens. Naam en
        geboortedatum zijn 5 jaar geldig. Je adres maar één jaar. Verversen kan
        altijd, wanneer jij maar wil. Bijvoorbeeld door direct na je 18e
        verjaardag je leeftijd up-to-date te maken in de app. Dan kan je meteen
        laten zien dat je 18+ bent. Hoe kan ik inloggen, of mijn gegevens inzien
        op de sites waar ik Yivi gebruik? Volg de uitleg op de websites zelf om
        in te loggen. Benieuwd naar het gebruik van je gegevens? Vraag dit dan
        op bij de website waar ‘t om gaat.
  - qa:
      question: Waar kan ik de QR-code vinden op de website van een organisatie?
      answer: Meestal vind je deze via een knop. Bijvoorbeeld ‘inloggen’ of ‘gegevens
        delen’. Soms ondersteunt een organisatie Yivi (nog) niet. Neem dan
        contact op met de organisatie.
  - qa:
      question: Hoe scan ik Yivi's QR-code?
      answer: Open eerst je Yivi-app. En druk onderin de app op ‘Scan QR’. Richt je
        telefoon op de QR-code die je wil scannen. Soms vraagt je telefoon om
        toestemming om je camera te mogen gebruiken. Zodra je die geeft kan je
        starten met scannen.
  - qa:
      question: Kan ik Yivi ook in het buitenland gebruiken?
      answer: Jazeker! Yivi kun je overal gebruiken. Steeds meer websites uit het
        buitenland sluiten zich bij ons aan.
  - qa:
      question: Wat bedoelen jullie met 'betrouwbaarheidsniveau' bij persoonsgegevens?
      answer: Gebruik je DigiD om gegevens in te laden in Yivi? Dan staat je
        beveiligingsniveau standaard op ‘Midden’. Wil je daar nog een schepje
        bovenop? Koppel dan je paspoort of ID aan je DigiD-app. Haal daarna de
        gegevens opnieuw op in Yivi. Meer lezen? Kijk op
        [www.digid.nl](https://www.digid.nl/).
  - qa:
      question: Hoe pas ik mijn adres (of andere gegevens) aan?
      answer: De organisatie waar je de gegevens ophaalt, bepaalt wat erin staat.
        Veranderen je gegevens? Geef dit altijd door aan de gemeente. Zij voeren
        deze veranderingen door. Daarna kun jij je nieuwe gegevens weer bij hen
        ophalen, en in Yivi zetten.
  - qa:
      question: De Yivi-knop op een site opent de link naar de app niet. Wat nu?
      answer: Vervelend! Gelukkig is dit vaak makkelijk op te lossen. Gebruik je
        Firefox op een Android telefoon? Pas dan je instellingen aan. Firefox
        opent namelijk standaard geen links van andere apps. Aanpassen doe je
        via ‘instellingen’. Ga naar ‘Geavanceerd’ en kies de optie ‘Openen van
        koppelingen in apps’. Gebruik je een andere browser, of een ander type
        telefoon. Dan kan het zijn dat de link niet werkt. Dit ligt dus niet aan
        jou. Neem contact op met de beheerder van de site om de link te laten
        herstellen. Lukt dit niet? Neem dan contact op met ons via
        [support@yivi.app](mailto:support@yivi.app).
  - qa:
      question: Wat zijn de minimale eisen voor mijn mobiel om de Yivi-app te gebruiken?
      answer: >-
        Om Yivi te gebruiken heb je voor een:




        * **iPhone** minimaal het besturingssysteem versie iOS 12.0 nodig. Deze is beschikbaar voor iPhone 5s en hoger.

        * **Android** minimaal het besturingssysteem Android 6 nodig en het toestel moet een processor met de ARM, ARM64 of x86-64 architectuur hebben.
  - qa:
      answer: Zeker weten! Dit raden we zelfs aan. Download de app, en vul je gegevens
        in. Net zoals je op je ‘eerste’ telefoon gedaan hebt. Als je die kwijt
        raakt, kan je Yivi via je tweede telefoon gebruiken.
      question: Kan je Yivi ook op een andere telefoon installeren?
  - qa:
      answer: Je kunt de Yivi-app installeren op je nieuwe telefoon. Daarna moet je je
        gegevens opnieuw ophalen. Dat is in een handomdraai gedaan. Je kan
        helaas nog geen gegevens overzetten van de ene naar de andere Yivi-app.
      question: Ik heb een nieuwe telefoon. Wat nu?
  - qa:
      question: Ik ben mijn telefoon kwijt! Wat nu?
      answer: Vervelend! Als je je mobiel verliest kan niemand in je Yivi-app. Want je
        hebt deze goed beveiligd met je pincode. Heb je je e-mailadres opgegeven
        via Mijn Yivi? Dan kan je op afstand de app blokkeren. Zo zijn je
        gegevens 100% veilig. Heb je Yivi al op een ander toestel geïnstalleerd?
        Dan kan je gewoon blijven inloggen op sites die Yivi ondersteunen.
  - qa:
      question: Ik wil Yivi en al mijn gegevens verwijderen. Hoe doe ik dat?
      answer: Via Mijn Yivi verwijder je zelf al je gegevens. Verwijder daarna de
        Yivi-app van je telefoon. Gebruik je de app een jaar lang niet? Dan
        verwijderen we na een jaar automatisch al je gegevens uit de app op je
        telefoon. Natuurlijk geven we je een seintje voor we dat doen.
  - qa:
      question: Ik heb een vraag over Yivi. Waar kan ik die stellen?
      answer: Heb je een vraag over inloggen met Yivi bij een winkel of organisatie?
        Dan kan je ‘t beste bij de winkel of organisatie terecht. Kom je er niet
        uit of heb je een andere vraag? Stuur dan een bericht via
        [support@yivi.app](mailto:support@yivi.app). Geef meteen even aan waar
        je tegenaan loopt, en welk model telefoon je gebruikt. Dan kunnen we je
        zo snel mogelijk helpen.
  - qa:
      question: Wat is het verschil tussen Yivi en andere ID-oplossingen zoals DigiD,
        iDIN, of Facebook?
      answer: Het grote verschil tussen Yivi en andere aanbieders is de decentrale
        structuur die wij gebruiken. Ingewikkelde term om aan te geven dat
        gegevens alleen op jouw telefoon staan, en nergens op een centraal punt
        opgeslagen zijn. Bij andere organisaties gebeurt dit wel. Daarom kunnen
        zij (ongewild) veel informatie over je verzamelen. Bij Facebook maken ze
        er zelfs een profiel mee, om je gerichte advertenties te sturen. Bij
        Yivi kijkt niemand mee. Ook de organisatie achter de Yivi-app niet. Want
        jij bent de baas over je gegevens, en niemand anders.
  - qa:
      question: Hoe werkt Yivi?
      answer: >-
        Voor het proces heb je 3 tools nodig:




        **Yivi-button op je website** Om je klant te inloggen met Yivi.


        **Yivi-serverapplicatie** De server is de spil tussen je website en je klant. Na het klikken op de inlogknop maakt de server een sessie aan die een QR-code genereert. Daarmee kan de connectie gelegd worden met de Yivi-app van de klant.


        **Yivi-app op de mobiel van de klant**. De identity wallet toont welke gegevens jij vraagt om in te loggen of jouw dienst te ontsluiten. De klant bepaalt of ie deze gegevens deelt om veilig en privacyvriendelijk gebruik te maken van jouw diensten.
---
