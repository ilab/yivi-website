---
layout: terms_and_conditions
title: Terms and Conditions
text: When you choose to use the Yivi app, these General Terms and Conditions
  apply. This version 2 is valid from 01-11-2024 onwards.
sections:
  - condition:
      title: 1. Acceptance of General Terms and Conditions of Use and applicability
      content: These General Terms and Conditions of Use apply to use of the Yivi app
        and the associated services. They form an agreement between you, as user
        of the Yivi app, and us, the Privacy by Design Foundation, in
        cooperation with Caesar Group and SIDN, as publisher of the app and
        operator of the Yivi system. The first time you use the Yivi app, you
        have to agree to these General Terms and Conditions of Use before you
        can proceed. If you don't agree with the General Terms and Conditions of
        Use, or decide later that you no longer agree, you shouldn't continue
        installing and using the Yivi app. As long as you continue using the
        Yivi app, the General Terms and Conditions of Use (including any
        amendments) will remain applicable.
  - condition:
      title: 2. Definitions
      content: >-
        **Yivi app**: a (mobile) application provided by Privacy by Design,
        Caesar Group and SIDN.., with which the user can retrieve attributes
        from Issuers, store them securely and confidentially on a mobile device,
        and share them selectively with product and service providers that use
        Yivi to request attributes.


        **Attribute**: a small package of data that typically states a characteristic of its owner (e.g. 'over 18'). Examples include the owner's bank account number, name, postal address, e-mail address, mobile number or Public Service Number (BSN).


        **Issuer**: an attribute provider.


        **Verifier**: a product or service provider that wishes to confirm one or more of a customer's or user's attributes.


        **[Privacy by Design Foundation](https://privacybydesign.foundation/en)**: registered with the Chamber of Commerce under Trade Register Number  67144128.


        **[Caesar Group](https://caesar.nl/)**: private company Zonnebaan Investments, at Utrecht, registered with the Chamber of Commerce under Trade Register Number  30185179, 100 % daughter of Caesar Group Rotterdam B.V.


        **[SIDN](https://www.sidn.nl/en)**: SIDN Business B.V., of Meander 501, 6825 MD Arnhem, The Netherlands, registered with the Chamber of Commerce under Trade Register Number 75364689.


        Within the legal framework of the European General Data Protection Regulation (GDPR), Privaccy by Design and Caesar Group are jointly data controllers and Caesar Group and SIDN are data processors. Caesar is taking over the activities of SIDN, after which SIDN will no longer be processor, probably early 2025.
  - condition:
      title: 3. What is Yivi?
      content: >-
        Yivi is a smartphone app, which works like an electronic passport with
        your personal data recorded in it, also called an identity wallet. You
        can use it to log in to a service, or to prove who you are. With Yivi,
        you never share more data than you need to. You can prove that you're
        over 18, for example, without giving your full date of birth. Or that
        you live in a particular town or city, without giving your exact
        address. With Yivi, you decide what data from your electronic passport
        you share with whom. The Yivi app is available from the Apple App Store,
        the Google Play App Store and the F-Droid App Store.


        You can also use Yivi to sign electronic documents. When you sign with Yivi, you share only relevant information, bundled into an 'electronic stamp'. With Yivi you can sign as a qualified doctor or as a citizen with your public service number (BSN), for example.
  - condition:
      title: 4. How does Yivi work?
      content: >-
        **4.1** Yivi works on a decentralised model. In other words, your data
        isn't held centrally. Using your own Yivi app, you obtain attributes
        from an Issuer, store them on your phone, and share them with Verifiers
        if and when you wish. Your attributes aren't kept anywhere else than on
        your phone, and can't be viewed or shared except using the Yivi app.
        What's more, no one else, not even the organisations running Yivi, can
        see which Issuers you get attributes from, or which Verifiers you share
        them with. They can't tell what attributes you obtain or share, either.
        The whole system is designed for privacy.


        **4.2** Yivi can be used for things such as:


        * Logging in

        * Verifying personal information, such as your age or where you live

        * Auto-completing forms

        * Signing documents


        **4.3** Yivi uses open-source software, ensuring complete transparency about what it does and what happens to your data.
  - condition:
      content: >-
        **5.1** Yivi is a semi-open system. Anyone can become a Yivi Verifier
        and ask you to provide information about yourself using the Yivi app.
        The operators of the Yivi system cannot control who acts as a Yivi
        Verifier. Nor do they supervise Verifiers. It's always your
        responsibility to check who is asking for your personal data, and to
        decide whether you wish to share your data with them or not.


        **5.2** Not everyone can become a Yivi Issuer. Connection to the system happens only via a contract with the operators of Yivi, that defines the nature of the data that the Issuer provides. When you obtain data from an Issuer, that's a transaction between you and the Issuer. It's up to you and the Issuer to make sure that you are entitled to the data and that the data is correct. The Yivi operators play no part in that.
      title: 5. Semi-open system and role of SIDN
  - condition:
      title: 6. Loss, reset or replacement of phone or PIN
      content: >-
        **6.1** Attributes that you obtain from Issuers using the Yivi app are
        stored on your phone and nowhere else. They're stored in encrypted form,
        so they can be viewed and shared only using the Yivi app on that phone.
        That has the following consequences if you lose, reset or replace your
        phone, or if you forget the PIN for the app:


        * If you lose your phone, or if it's stolen, the app remains protected by the PIN. To make certain that no one who gets hold of your phone can use the app, you can block it remotely, providing that you've linked an e-mail address to your Yivi app.

        * If you get a new phone, or fully reset your existing phone, or use the app settings to fully reset the app, you'll need to re-activate the app to continue using it. The attributes you had on your old phone or before the reset will be lost. You will therefore have to reload them from the Issuers.

        * If you forget the PIN for your app, you won't be able to access the attributes on your phone. You'll need to download the app again, re-activate it and get your attributes from the Issuers again.
  - condition:
      title: 7. What does Yivi cost?
      content: >-
        **7.1** For you, the user, downloading and using the Yivi app doesn't
        cost anything; the Yivi app itself is free.


        **7.2** Many Issuers make attributes available free as well. However, it's up to each Issuer whether to charge for issuing information, and some may choose to do so.
  - condition:
      title: 8. What information about you do the Yivi operators have?
      content: >-
        **8.1** The attributes that you keep in the Yivi app on your phone (or
        tablet) aren't stored anywhere else by the Yivi operators. Nothing is
        kept in the cloud, for example, or in a central database maintained by
        Yivi or anyone else.


        **8.2** When you use the Yivi app to get information from an Issuer or to share it with a Verifier, the Yivi app on your phone communicates directly with the Issuer's or Verifier's system. So the Yivi operators know nothing about what information you have obtained where, or who you share it with.


        **8.3** When an operator acts as Issuer of Attributes, such as your mobile number and e-mail address, this operator uses your data only to supply the relevant attributes to the Yivi app.


        **8.4** In order to enable you to keep track of your use of the Yivi app, the app sends timestamps to the operator, which are stored. The timestamps say when the Yivi app was used. So the operator knows when the app on your phone was active. But the operator doesn't how who was using the app, or what for. The operator uses the timestamp information only to let you see when your Yivi app was used, and to enable you to block the app if someone else gets access to it. The aim is to optimise the security of the Yivi app and to assure your privacy by design.


        **8.5** The operators saves the timestamps for as long as there's data in your Yivi app. If you delete all the data from the app, the timestamps are kept for another 14 days so that you don't immediately lose your overview of use of the app. After 14 days, they are permanently deleted. If you don't use your Yivi app for a year, or do anything to keep your account active, all information about you will be deleted from the Yivi infrastructure.


        **8.6** The Yivi operator hosts the so-called 'keyshare server'. The server is a central element of the Yivi infrastructure, which is needed for Yivi to work properly, and to keep transactions secure. An encrypted version of your PIN is stored on the server, together with a cryptographic key linked to your phone. Each time you use your PIN – to unlock the Yivi app, confirm a transaction or agree to share certain data, for example – your Yivi app connects to the keyshare server. The app needs to do that in order to check your PIN, and doesn't connect to the server for any other purpose. The keyshare server isn't directly involved in any transaction you perform. So the operator doesn't know who you are transacting with or what the transaction involves. What's more, the operator can't actually see or access your PIN.


        **8.7** If you've chosen to link an e-mail address to your Yivi account, that address is saved by the operator. Details of how we use your personal data in connection with Yivi are given in the applicable Yivi Privacy Statement.
  - condition:
      title: 9. Liability
      content: >-
        **9.1** As a Yivi app user, you are personally responsible for obtaining
        your attributes from Issuers. It's also up to you to check that any
        attributes provided by an Issuer are correct. If you find a mistake, you
        should contact the Issuer to get your data changed before using it in
        the Yivi app.


        **9.2** As a Yivi app user, you're also responsible for what you do and don't choose to share with Verifiers. The Yivi operator isn't responsible for how Verifiers use your attributes, and we don't vet data recipients to confirm their trustworthiness. Each time a Verifier asks for your data, the Yivi app asks for your explicit consent before the relevant attributes are shared.


        **9.3** As a Yivi app user, you are responsible for setting your own secure PIN, and for keeping it secret. The Yivi app is a strictly personal utility for the storage of personal data. It's up to you to make sure that the app remains under your control, so that no one else has access to your data.


        **9.4** Privacy by Design, Caesar Group and SIDN are the publishers of the Yivi app and the operators of the Yivi system. They make every reasonable effort to ensure that the app and the system remain available, that attributes can be obtained from Issuers and saved confidentially in the app, and that attributes or information derived from them can be shared with Verifiers. However,they offer no availability or performance guarantees. Neither they, nor any third party engaged by them, are liable for any damages suffered as a direct or indirect consequence of availability issues affecting the Yivi app or faults in (updates to) the app, or as a consequence of the app's use.
  - condition:
      title: 10. Amendments to the Yivi General Terms and Conditions of Use and
        modifications to the Yivi app
      content: >-
        **10.1** The Yivi operators may unilaterally amend or update these
        General Terms and Conditions of Use at any time. Any amendment or update
        will be announced in the Yivi app at least 2 months before it comes into
        effect. The first time that you open the Yivi app following an amendment
        to the General Terms and Conditions of Use, you will be told about the
        amendment, and you will need to accept the amended version in order to
        continue using the app.


        **10.2** As a Yivi app user, you must always use the latest available version of the app. You are therefore required to instal each update that is released. If you don't agree to instal an update within a reasonable time, you will lose your right to continue using the app.
  - condition:
      title: 11. Cessation of the service
      content: >-
        **11.1** The Yivi operators reserve the right to modify the capabilities
        of the Yivi app or to withdraw the app from service at any time.


        **11.2** The operators may also at any time decide to cease the publication of the Yivi app and the release of updates to it.


        **11.3** If the operators decide to cease the publication of the Yivi app, the cessation will be announced in the Yivi app at least 6 months beforehand.
  - condition:
      title: 12. Applicable law and competent court
      content: >-
        **12.1** These General Terms and Conditions of Use are governed
        exclusively by Dutch law.




        **12.2** Notwithstanding any provision to the contrary in applicable consumer law, a dispute regarding these General Terms and Conditions of Use/this agreement or the Yivi app shall be referred to the competent court in Amsterdam.




        **12.3** The English-language version of these General Terms and Conditions of Use is a translation of an original Dutch-language text. In the event of any discrepancy between the two, the Dutch version shall prevail.
---
