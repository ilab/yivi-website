---
layout: terms_and_conditions
title: Algemene voorwaarden
text: Wil je de Yivi.app gebruiken dan gelden deze algemene voorwaarden. Deze
  versie 2 is geldig vanaf 01-11-2024.
sections:
  - condition:
      title: 1. Aanvaarding van de algemene gebruikersvoorwaarden en toepasselijkheid
      content: Deze algemene gebruikersvoorwaarden zijn van toepassing op het gebruik
        van de Yivi-app en de dienstverlening. Deze vormen de overeenkomst
        tussen jou als gebruiker van de Yivi-app en de Stichting Privacy by
        Design, in samenwerking met Caesar Groep en SIDN, als uitgever van de
        app en beheerder van het Yivi-stelsel. Bij het eerste gebruik van de
        Yivi-app dien je, voordat je verder gebruik kunt maken van de app,
        akkoord te gaan met deze algemene gebruikersvoorwaarden Yivi. Indien je
        (op enig moment) niet akkoord gaat met de algemene gebruikersvoorwaarden
        Yivi, onthoud jezelf dan van het installeren en gebruiken van de
        Yivi-app. Zolang je de Yivi-app gebruikt blijven de algemene
        gebruikersvoorwaarden (inclusief wijzigingen) van toepassing.
  - condition:
      title: 2. Definities
      content: >-
        **Yivi- app**: (mobiele) applicatie aangeboden door Privacy by Design,
        Caesar Groep en SIDN. waarmee de gebruiker attributen kan ophalen bij
        Issuers. Deze attributen veilig en vertrouwelijk op het mobiele apparaat
        kan opslaan en vervolgens naar eigen keuze kan delen met partijen die
        Yivi gebruiken om deze attributen op te vragen.


        **Attributen**: een klein stukje data dat doorgaans een verklaring bevat over de eigenaar van het attribuut (bijv. ‘> 18 jaar’).Een attribuut is bijvoorbeeld je banknummer, naam, huisadres, e-mailadres, 06-nummer of Burgerservicenummer (BSN).


        **Issuer**: een uitgever van attributen.


        **Verifier**: een partij die iemands attributen wil verifiëren om aan de gebruiker een dienst te verlenen of een product te leveren.


        **[Stichting Privacy by Design](https://privacybydesign.foundation/)**: statutair gevestigd te Nijmegen, ingeschreven bij de Kamer van Koophandel onder nummer 67144128.


        **[Caesar Groep](https://caesar.nl/)**: besloten vennootschap Zonnebaan Investments B.V., gevestigd en kantoorhoudende te Utrecht,  ingeschreven bij de Kamer van Koophandel onder nummer 30185179, 100 % dochter van Caesar Groep Rotterdam B.V.


        **[SIDN](https://www.sidn.nl/)**: SIDN Business B.V., statutair gevestigd te Arnhem en kantoorhoudende te 6825 MD Arnhem aan Meander 501, ingeschreven bij de Kamer van Koophandel onder nummer 75364689.


        Binnen het juridische raamwerk van de Algemene Verordening Gegevensbescherming (AVG) zijn Privacy by Design en Caesar Groep gezamenlijk verwerkingsverantwoordelijken en zijn Caesar Groep en SIDN gezamenlijk verwerkers. Caesar neemt de activiteiten over van SIDN, waarna SIDN niet langer verwerker zal zijn, vermoedelijk in het begin van 2025.
  - condition:
      title: 3. Wat is Yivi?
      content: >-
        Yivi is een app die je op je mobiele telefoon kunt installeren. Yivi kun
        je zien als een digitale portemonnee met je persoonsgegevens. Dat wordt
        ook wel een *identity wallet* genoemd. Met je gegevens in Yivi kun je
        inloggen bij een organisatie of aantonen wie je bent. Je deelt niet meer
        gegevens dan nodig: je bewijst bijvoorbeeld dat je ouder bent dan 18
        jaar zonder je geboortedatum prijs te geven of dat je in een bepaalde
        buurt woont, zonder je exacte woonadres prijs te geven. Met Yivi bepaal
        je als gebruiker dus zelf welke gegevens je deelt uit je portemonnee en
        met wie. De Yivi-app is beschikbaar via de Apple App Store, de Google
        Play App Store en de F-Droid App Store.


        Ook kun je met Yivi documenten digitaal ondertekenen. Je gebruikt alleen relevante kenmerken van jezelf, in een soort digitaal stempel. Zo kun je met Yivi ondertekenen als arts, als burger met je BSN, of vanuit een andere rol.
  - condition:
      title: 4. Hoe werkt Yivi?
      content: >-
        **4.1** Yivi werkt aan de hand van een decentraal model. Dit houdt in
        dat de gebruiker eerst attributen ophaalt in de eigen Yivi-app bij een
        Issuer waarna de gebruiker zijn gegevens kan delen met een Verifier. De
        opgehaalde attributen worden nergens anders opgeslagen dan op je
        telefoon en kunnen alleen via de Yivi-app geraadpleegd en gedeeld
        worden. Ook kan niemand, ook de organisaties achter Yivi niet, zien bij
        welke Issuer de gebruiker attributen ophaalt of met wie de gebruiker
        attributen deelt. Ook niet welke attributen worden opgehaald of gedeeld.
        Het hele stelsel is op privacy by design gebaseerd.


        **4.2** Je kunt Yivi gebruiken voor toepassingen als:


        * Inloggen.

        * Verifiëren van bepaalde persoonlijke gegevens (bijv. leeftijd of woonplaats).

        * Automatisch invullen van formulieren.

        * Ondertekenen van documenten.


        **4.3** Yivi draait op open source software en het is daarmee volledig transparant wat het doet en wat er met jouw gegevens gebeurt.
  - condition:
      title: 5. Deels open stelsel en rol SIDN
      content: >-
        **5.1** Yivi is een deels open stelsel. Iedereen die dat wil kan Yivi
        Verifier worden en jou vragen via Yivi gegevens te verstrekken. De
        beheerders van het Yivi-stelsel kunnen niet beperken welke partijen als
        Verifier van het Yivi-stelsel gebruik maken. De beheerders van het
        stelsel hebben dus ook geen toezicht op Verifiers. Het is daarom in alle
        gevallen jouw verantwoordelijkheid om te controleren met wie je welke
        gegevens deelt en of je dat wel of niet wil.


        **5.2** Niet iedereen kan Yivi Issuer worden. Aansluting op het stelsel als Isssuer gebeurt alleen via een  overeenkomst met de beheerders van Yivi waarin beschreven staat wat de betreffende Issuer voor gegevens uitdeelt. Het ophalen van gegevens bij een Issuer is een transactie tussen jou en de Issuer. Of je de gegevens mag opvragen en of je de juiste en correcte gegevens ontvangt is tussen jou en de Issuer. Ook daarin spelen de beheerders geen rol.
  - condition:
      title: 6. Verlies, reset of nieuwe telefoon en vergeten pincode
      content: >-
        **6.1** De attributen die je via de Yivi-app hebt opgehaald bij Issuers
        staan uitsluitend op je telefoon en zijn daar versleuteld opgeslagen
        zodat ze alleen via de Yivi-app op die telefoon bekeken en gedeeld
        kunnen worden. Dit heeft de volgende consequenties als je je telefoon
        verliest, reset, een nieuwe telefoon hebt of je de pincode van de app
        kwijt bent:




        * In het geval je je mobiel verliest of deze gestolen wordt, blijft de app beschermd door de pincode. Om er zeker van te zijn dat niemand toch toegang tot de app krijgt, kun je de app op afstand blokkeren wanneer je gebruik hebt gemaakt van de optie in de Yivi-app om een e-mailadres toe te voegen.

        * Als je een nieuwe telefoon in gebruik neemt, een bestaande telefoon helemaal gereset hebt of vanuit de instellingen in de app alles gereset hebt, zul je de app opnieuw moeten activeren. Je bent dan al jouw attributen kwijt en zal ze opnieuw bij de Issuers op moeten halen.

        * Als je je pincode van de app niet meer weet, kun je niet meer bij de attributen. Je zult dan de app opnieuw moeten downloaden, activeren en vervolgens de attributen opnieuw moeten verzamelen door deze op te halen bij de Issuers.
  - condition:
      title: 7. Wat kost Yivi?
      content: >-
        **7.1** Aan het downloaden en het gebruik van de Yivi-app zijn voor de
        gebruiker geen kosten verbonden: de Yivi-app is gratis.


        **7.2** Het opvragen van attributen bij Issuers is in veel gevallen ook gratis. Het kan echter voorkomen dat een Issuer kosten verbindt aan het uitgeven van informatie. Dat is een vrije keuze van de Issuer.
  - condition:
      title: 8. Welke informatie hebben de Yivi beheerders wel en niet over je?
      content: >-
        **8.1** Jouw attributen die je in de Yivi-app verzamelt staan alleen in
        de app op jouw eigen telefoon (of- tablet) en worden door de Yivi
        beheerders nergens anders opgeslagen. Ze staan dus niet ook nog ergens
        in een cloud of centraal in de computersystemen van Yivi of een derde. 


        **8.2** Wanneer je de Yivi-app gebruikt om gegevens bij een Issuer op te halen of met een Verifier te delen, communiceert de Yivi-app direct met de systemen van de Issuer dan wel de Verifier. De Yivi beheerders kunnen niet zien welke gegevens je ophaalt of deelt en ook niet bij welke Issuer je de data ophaalt of met welke Verifier je je gegevens deelt.


        **8.3** Wanneer een beheerder Issuer is van Attributen, zoals het geval is bij het mobiele nummer of e-mail adres, gebruikt deze beheerder de gegevens enkel voor het leveren van het desbetreffende Attribuut in de Yivi-app.


        **8.4** Om jou als gebruiker van de Yivi-app inzicht te kunnen geven in jouw app-gebruik ontvangt de beheerder vanuit de app tijdstempels die door de beheerder opgeslagen worden. Die tijdstempels geven weer op welk moment de Yivi-app is gebruikt. Dit wil zeggen dat de beheerder kan zien op welk tijdstip er via een bepaalde app een activiteit heeft plaatsgevonden. De beheerder kan niet zien wat er heeft plaatsgevonden of waar de Yivi-app is gebruikt en weet ook niet wie de gebruiker van de app is. De beheerder gebruikt deze informatie alleen om jou als gebruiker de mogelijkheid te bieden om te zien wanneer je Yivi-app is gebruikt en om bij eventueel misbruik van je Yivi-app, het gebruik van de app te beëindigen. Op deze manier wordt optimale beveiliging van de Yivi-app nagestreefd en biedt Yivi, by design, optimale privacybescherming.


        **8.5** De tijdstempels worden door de beheerder bewaard zolang er gegevens in de Yivi-app staan. Na het verwijderen van je gegevens in de Yivi-app worden de tijdstempels nog 14 dagen bewaard. Binnen deze 14 dagen heb je nog de mogelijkheid om een overzicht van je Yivi-activiteiten te bekijken. Daarna worden de tijdstempels permanent verwijderd. Wanneer je geen actie onderneemt en je Yivi-app een jaar lang niet gebruikt, worden je gegevens na dat jaar automatisch verwijderd van de Yivi infrastructuur.


        **8.6** Voor de correcte werking van Yivi en voor de beveiliging van de transacties en van de gebruiker, host de Yivi beheerder een centraal component: de Keyshare server. Op deze server wordt de PIN-code van de gebruiker versleuteld opgeslagen, inclusief een cryptografische sleutel die gekoppeld is aan de telefoon van de gebruiker. Bij elke actie waarbij een PIN-code gebruikt wordt (bijvoorbeeld bij het ontgrendelen van de Yivi-app, het bevestigen van een transactie en bij het goedkeuren van het delen van gegevens) raadpleegt de Yivi-app de Keyshare server. Het raadplegen van de Keyshare server vindt alleen plaats en is noodzakelijk om de controle van de PIN-code mogelijk te maken. De Keyshare server maakt zelf geen deel uit van de transacties. De Yivi beheerder ziet niet met wie er een transactie is opgezet – of – wat de inhoud van de transactie is. Daarnaast is de beheerder niet in staat om de PIN-code te lezen of te openen.


        **8.7** Indien je in de Yivi-app een e-mailadres hebt opgegeven, dan wordt dit e-mailadres opgeslagen door de Yivi beheerder. Meer details over hoe SIDN in het kader van Yivi met je persoonsgegevens omgaat is beschreven in de toepasselijke Yivi Privacyverklaring.
  - condition:
      title: 9. Aansprakelijkheid
      content: >-
        **9.1** Als gebruiker van de Yivi-app ben je zelf verantwoordelijk voor
        het ophalen van jouw attributen bij een Issuer. Ook is het jouw
        verantwoordelijkheid om na te gaan of jouw attributen die een Issuer
        uitgeeft juist zijn. Indien je gegevens niet kloppen dien je contact op
        te nemen met de Issuer om je gegevens bij hen te laten wijzigen alvorens
        je de gegevens in de Yivi-app gaat gebruiken.


        **9.2** Als gebruiker van de Yivi-app ben je zelf verantwoordelijk voor welke gegevens je wel en of niet verstrekt aan een Verifier. De Yivi beheerder is niet verantwoordelijk voor het gebruik van attributen door een Verifier en controleert niet op de betrouwbaarheid van de ontvangende partijen. Als gebruiker geef je zelf in de Yivi-app steeds expliciet toestemming voordat deze kenmerken vanuit de Yivi-app doorgegeven worden.


        **9.3** Het is de verantwoordelijkheid van de gebruiker van de Yivi-app om een veilige en persoonlijke pincode in te stellen en deze geheim te houden. De Yivi-app is strikt persoonlijk, bevat jouw persoonsgegevens en het is aan jou om te zorgen dat deze gegevens alleen onder jouw controle via de app worden gedeeld.


        **9.4** Privacy by Design, Caesar Groep en SIDN zijn uitgevers van de Yivi-app en beheren het Yivi-stelsel. ZIj spannen zich daarbij in om de app en het stelsel beschikbaar te houden en de mogelijkheid te bieden attributen op te halen bij Issuers, binnen de app vertrouwelijk op te slaan en Attributen of informatie op basis van de attributen met Verifiers te delen. Zij bieden hierover geen garanties. Zij, en door haar ingeschakelde derde(n), zijn niet aansprakelijk voor directe of indirecte schade welke is ontstaan door storingen in de beschikbaarheid of fouten in (updates van) de Yivi-app of het gebruik daarvan.
  - condition:
      title: 10. Wijziging van de algemene gebruikersvoorwaarden Yivi en de Yivi-app
      content: >-
        **10.1** De Yivi beheerders kunnen op elk ogenblik de algemene
        gebruikersvoorwaarden Yivi eenzijdig wijzigen of updaten. De beheerders
        zullen wijzingen of updates van de algemene gebruikersvoorwaarden Yivi
        binnen een redelijke termijn, maar minimaal 2 maanden voor de
        inwerkingtreding daarvan, in de Yivi-app kenbaar maken. Indien de
        algemene gebruikersvoorwaarden zijn gewijzigd krijg je dit bij het
        openen van de Yivi-app te zien en moet je de gewijzigde voorwaarden
        accepteren om de Yivi-app te kunnen blijven gebruiken.


        **10.2** Als gebruiker van de Yivi-app ben je verplicht om de nieuwste versie (updates) van de app te installeren. Indien je hier niet mee instemt, dan vervalt na enige tijd jouw recht om de app te gebruiken.
  - condition:
      title: 11. Beëindiging van de dienst
      content: >-
        **11.1** De Yivi beheerders behouden zich het recht voor om te allen
        tijde de gebruiksmogelijkheden van de Yivi-app te wijzigen of te
        beëindigen.


        **11.2** De beheerders kunnen op elk tijdstip besluiten om geen updates meer uit te brengen en de Yivi-app niet langer aan te bieden.


        **11.3** Indien de beheerders op enig moment besluiten om de Yivi-app niet langer aan te bieden, dan zullen zij dit 6 maanden voor het beëindigen van dienst aankondigen in de Yivi-app.
  - condition:
      title: 12. Toepasselijk recht en bevoegde rechter
      content: >-
        **12.1** Op deze algemene gebruikersvoorwaarden is uitsluitend
        Nederlands recht van toepassing.




        **12.2** Behoudens de andersluidende toepasselijke bepalingen in het consumentenrecht, worden geschillen die ontstaan met betrekking tot deze algemene gebruikersvoorwaarden/overeenkomst of de Yivi-app voorgelegd aan de rechter te Amsterdam.




        **12.3** Van de Nederlandse versie van deze algemene gebruikersvoorwaarden Yivi is een Engelse vertaling gemaakt. Als blijkt dat de Engelse vertaling qua inhoud afwijkt van de Nederlandse versie, heeft de Nederlandse versie voorrang.
---
