---
layout: cookies
title: Cookies
text: Yivi’s website makes use of cookies. Cookies are small files or blocks of
  data, which are saved on your computer, smartphone or tablet PC when you visit
  yivi.app. The cookies that we use enable us to analyse the way visitors browse
  our site, so that we can improve it. The data that we collect with cookies
  cannot be traced back to individual users. The various cookies that we use are
  listed below. Information about how to disallow cookies is also provided.
cookies:
  - cookie:
      name: Cookie name
      purpose: What does the cookie do?
      lifespan: Lifespan of the cookie on your device
  - cookie:
      name: Load balancer cookie
      purpose: Records which web server you used when visiting the website, so that
        you can be connected to the same server next time. Connecting to the
        same server enables the site to be presented more quickly and means that
        your settings can be retained.
      lifespan: For the length of your browser session. The cookie is deleted when you
        close your browser.
  - cookie:
      name: Web application Firewall
      purpose: Protects web applications by monitoring and filtering traffic.
      lifespan: " "
  - cookie:
      name: rbzid and rbxsessionid
      purpose: Is used to perform a challenge - ie see if the user is a valid user
      lifespan: Exists during your session and will be removed after session is closed.
  - cookie:
      name: PiwikPro
      purpose: Gathers anonymous/ aggregated information about where you go and what
        you do on the site.
      lifespan: Persistent
  - cookie:
      name: YouTube
      purpose: For opening the video via YouTube in frame
      lifespan: " "
refuse_title: Disallowing cookies
refuse_content: "You can stop our website from saving cookies on your device if
  you wish. To do so, open the ‘Settings’ menu in your browser and then follow
  the instructions for removing cookies ([Internet
  Explorer](http://windows.microsoft.com/nl-NL/windows-vista/Block-or-allow-coo\
  kies), [Safari](http://support.apple.com/kb/PH5042), [Google
  Chrome](https://support.google.com/chrome/answer/95647?hl=nl), [Mozilla
  Firefox](https://support.mozilla.org/nl/kb/cookies-in-en-uitschakelen-website\
  s-voorkeuren?redirectlocale=nl&redirectslug=Cookies+in-+en+uitschakelen),
  [Opera](http://help.opera.com/Linux/10.10/nl/cookies.html)). NB: if you use
  more than one browser, you will need to disallow cookies in each browser
  separately."
---
