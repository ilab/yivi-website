---
layout: cookies
title: Cookies
text: De website van Yivi plaatst cookies. Dit zijn kleine informatiebestandjes
  die worden bewaard op jouw computer, tablet of mobiele telefoon. Via deze
  cookies analyseren wij het zoekgedrag op onze website en verbeteren wij jouw
  gebruiksgemak. De data die wij verzamelen via cookies, is niet te herleiden
  naar een individu.
cookies:
  - cookie:
      name: Cookie naam
      purpose: Wat doet de cookie?
      lifespan: Levensduur cookie op uw apparaat
  - cookie:
      name: Load balancer cookie
      purpose: Onthoudt welke webserver je gebruikt bij jouw bezoek aan de website,
        met als doel je bij een volgend bezoek naar dezelfde server te leiden.
        Op deze manier kunnen wij je de site sneller presenteren en blijven
        instellingen die je gedaan hebt behouden.
      lifespan: Gedurende je sessie.De cookie wordt verwijderd zodra je je browser
        afsluit.
  - cookie:
      name: Web Application Firewall
      purpose: Beschermt webapplicaties door het monitoren en filteren van verkeer.
      lifespan: " "
  - cookie:
      name: rbzid en rbxsessionid
      purpose: Controleert of de gebruiker een echte gebruiker is
      lifespan: Gedurende je sessie. De cookie wordt verwijderd zodra je jouw browser
        afsluit.
  - cookie:
      name: PiwikPro analytics
      purpose: Biedt anonieme/ geaggregeerde informatie over waar je heen gaat en wat
        je doet op de site.
      lifespan: Blijvend
  - cookie:
      name: YouTube
      purpose: Voor het open in frame van de video's via YouTube
      lifespan: " "
refuse_title: Cookies weigeren
refuse_content: "Je kunt onze cookies weigeren als je dat wilt. Je kiest dan de
  optie ‘instellingen’ via jouw browser en volgt daar de instructies om de
  cookies te verwijderen ([Internet
  Explorer](http://windows.microsoft.com/nl-NL/windows-vista/Block-or-allow-coo\
  kies), [Safari](http://support.apple.com/kb/PH5042), [Google
  Chrome](https://support.google.com/chrome/answer/95647?hl=nl), [Mozilla
  Firefox](https://support.mozilla.org/nl/kb/cookies-in-en-uitschakelen-website\
  s-voorkeuren?redirectlocale=nl&redirectslug=Cookies+in-+en+uitschakelen),
  [Opera](http://help.opera.com/Linux/10.10/nl/cookies.html)). Let op: dit moet
  je voor iedere browser apart instellen."
---
