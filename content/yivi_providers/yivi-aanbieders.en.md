---
layout: yivi_providers
title: Yivi providers
sections:
  overview_yivi_providers:
    visibility: true
    title: Overview Yivi providers by sector
    content: At which organisations can you already use Yivi? Below is the overview
      of organisations divided into different sectors, such as municipality,
      care, education.
    list_of_providers:
      - provider:
          category: Municipalities
          content: Municipality Nijmegen Requesting data from the Key Register of Persons
            (BRP), login in 'Mijn Nijmegen' and voting with Yivi for referendum.
          image: /img/logo-gemeente-nijmegen.jpg
          imageAltText: Logo municipality Nijmegen
      - provider:
          category: Municipalities
          content: Yivi the new way to log in, watch the demos.
          image: /img/default_web_rgb.svg
          imageAltText: Logo municipality Amsterdam
      - provider:
          category: E-mail & file sharing
          content: PostGuard User-friendly end-to-end encryption for emails and files
            using Yivi.
          image: /img/logo_postguard.svg
          imageAltText: Logo PostGuard
---
