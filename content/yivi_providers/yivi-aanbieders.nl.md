---
layout: yivi_providers
title: Yivi aanbieders
sections:
  overview_yivi_providers:
    visibility: true
    title: Overzicht Yivi-aanbieders per sector
    content: Bij welke organisaties kun je al gebruik maken van Yivi? Hieronder zie
      het overzicht van de organisaties opgedeeld in verschillende sectoren,
      zoals gemeente, zorg, scholing.
    list_of_providers:
      - provider:
          category: Municipalities
          content: Gemeente Nijmegen Gegevens opvragen uit de Basisregistratie Personen
            (BRP), inloggen in 'Mijn Nijmegen' en stemmen met Yivi voor een
            referendum.
          image: /img/logo-gemeente-nijmegen.jpg
          imageAltText: Logo gemeente Nijmegen
      - provider:
          category: Municipalities
          content: Yivi de nieuwe manier van inloggen, bekijk de demo's.
          image: /img/default_web_rgb.svg
          imageAltText: Logo gemeente Amsterdam
      - provider:
          category: E-mail & file sharing
          content: PostGuard gebruiksvriendelijke end-to-end encryptie voor e-mails en
            bestanden met behulp van Yivi.
          image: /img/logo_postguard.svg
          imageAltText: Logo PostGuard
---
