---
title: Meetings
description: >-
  Twee of drie keer per jaar wordt er een Yivi bijeenkomst georganiseerd waar de
  laatste ontwikkelingen aan bod komen over digitale identiteit in het algemeen
  en over Yivi in het bijzonder. Er komen meestal tussen de 25 en 50 mensen uit
  het bedrijfsleven, de overheid en de wetenschap. Deelname is gratis. De sfeer
  is open en inhoudelijk gericht. De locatie is (bijna altijd) bij SURF (zie
  [adres en route](https://www.surf.nl/en/about/contact-with-surf)).


  Aankondigingen verschijnen via [Mastodon](https://mastodon.nl/@yivi_privacybydesign) en via de Yivi mailing lijst. Wil je daar aan toegevoegd worden, stuur dan een bericht naar: b.jacobs

  ‘at’ privacybydesign.foundation
meetingsList:
  - title: Yivi meeting vrijdag middag 7 feb. 2025, bij SURF
    program:
      - content: 13:30 Opening, welkom.
      - content: 13:30 - 14:00 Wendy van den Eeckhout (Gemeente Nijmegen), "Digitale
          identiteitsontwikkelingen bij de Gemeente Nijmegen".
        files:
          - filename: "[ slides ]"
            file: /img/wallet-pilots-nijmegen-7-2-2025.pdf
      - content: 14:00 - 14:30, Peter Eikelboom (SURF), "Wat kan een wallet met een
          digitale identiteit en verifiable credentials betekenen voor het
          onderwijs (en de arbeidsmarkt)?".
        files:
          - filename: "[ slides ]"
            file: /img/wat-kan-een-eduwallet-betekenen-voor-het-onderwijs-7-2-2025.pdf
      - content: 14:30 - 15:00 Koffie/thee.
      - content: 15:00 - 15:30, Jaap-Henk Hoepman (Radboud Universiteit),  "Perspectief
          op cryptografie voor wallets".
        files:
          - filename: "[ slides ]"
            file: /img/yivi-eidas-7-2-2025.pdf
      - content: 15:30 - 16:00, Tim Vermeulen (Alliander),  "Digitale handtekeningen in
          een professionele omgeving".
        files:
          - filename: "[ slides ]"
            file: /img/time-of-the-signs-7-2-2025.pdf
      - content: 16:00 - 16:30, Dibran Mulder Caesar Groep), "Yivi ontwikkelingen".
        files:
          - filename: "[ slides ]"
            file: /img/caesar-7-2-2025.pdf
      - content: 16:30 - 17:30 borrel, aangeboden door SURF.
  - title: "Yivi meeting: Vrijdagmiddag, 11 okt. 2024 bij SURF"
    program:
      - content: 13:30 Opening, welkom
      - content: 13:30 - 14:00, Sara Vahdati Pour (student, Nijmegen), *Keyvi,
          single-signon with Keycloak login authenticator using Yivi*
        files:
          - filename: "[ slides ]"
            file: /img/sara-vahdatipour-sso.pdf
      - content: 14:00 - 14:30, Robert van Altena ([VerID](https://ver.id/)), *Yivi in
          de verzekeringswereld*
        files:
          - filename: "[ slides ]"
            file: /img/robert-van-altena-yivi-verzekeringswereld.pdf
      - content: 14:30 - 15:00 Jan den Besten, Lian Vervoort, Dirk Doesburg (Nijmegen),
          *Yivi voor PubHubs en PubHubs voor Yivi*
        files:
          - filename: "[ slides van Jan en Lian ]"
            file: /img/jan-den-besten-pubhubs.pdf
          - filename: "[ slides van Dirk ]"
            file: /img/dirk-doesburg-diyivi.pdf
      - content: 15:00 - 15:30 pauze
      - content: 15:30 - 16:00, Sietse Ringers, *(Cryptographic) developments in EUDI
          wallets*[](https://privacybydesign.foundation/meeting-slides/slides-11-10-2024/sietse-ringers-eudi-wallets.pdf)
        files:
          - filename: "[ slides ]"
            file: /img/sietse-ringers-eudi-wallets.pdf
      - content: 16:00 - 16:45, Martijn Sanders, Ivar Derksen
          ([SIDN](https://sidn.nl/)), Dibran Mulder ([Caesar
          Groep](https://caesar.nl/)), *Yivi transitie van SIDN naar Caesar*
        files:
          - filename: "[ slides van Martijn en Ivar ]"
            file: /img/martijn-sanders-yivi-handover.pdf
          - filename: "[ slides van Dibran ]"
            file: /img/dibran-mulder-yivi-caesar.pdf
  - title: "Yivi meeting: Vrijdagmiddag, 10 nov. 2023 bij SIDN"
    description: "De bijeenkomst begint om 13:30, bij
      [SIDN](https://www.sidn.nl/over-sidn/contact). Let op de bijeenkomst is
      deze keer bij SIDN in Arnhem, en dus niet bij SURF in Utrecht, zoals
      gebruikelijk. De precieze locatie is: SIDN, Meander 501, 6825 MD Arnhem,
      Bedrijfsrestaurant 3e etage."
    program:
      - content: 13:30 Opening, welkom
      - content: 13:30 - 14:00, Joost van Dijk (Yubico), *FIDO en webgebaseerde digital
          identity wallets*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=wP9-y1OSmY0
      - content: 14:00 - 14:30, Theo Hooghiemstra (Hooghiemstra en Partners),
          *Authenticatie in de zorg*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=5S4pHvoHZdk&list=PL4oSeW0AbF-kf39Wk5CKZ_D2RiHJQnNsq&index=3
      - content: |-
          14:30 - 15:00 Peter Eikelboom (SIDN), *Europese en nationale
            identity ontwikkelingen*
        files:
          - filename: "[ video ]"
            file: https://youtu.be/De9c_LLH_hQ
      - content: 15:00 - 15:30 pauze
      - content: |-
          15:30 - 16:00, iHub team (Radboud), *Yivi handtekeningen voor
            files, mails & posts*
        files:
          - filename: "[ video ]"
            file: https://youtu.be/B5xXb6XJWhY
      - content: >-
          16:00 - 16:30, Job Doesburg (Radboud), *Maatregelen tegen overvraging
          in 
            SSI en Yivi*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=PHf2VbXaIe0
      - content: 16:30 - 17:00, Martijn Sanders (SIDN), *Yivi ontwikkelingen*
        files:
          - filename: slides
            file: https://youtu.be/y9pRk6fCO1M
      - content: "17:00: borrel, aangeboden door SIDN"
  - title: "Yivi meeting: Vrijdagmiddag, 26 mei 2023 bij SURF"
    program:
      - content: 13:30 Opening, welkom
      - content: |-
          13:30 - 14:00, Martijn van Dam (stichting datakluis), *De
            Nederlandse Datakluis*
      - content: |-
          14:00 - 14:45, Wendy van den Eeckhout en Marnix Dessing
            (gemeente Nijmegen), *Uitgifte en gebruik van attributen*
      - content: |-
          14:45 - 15:15 Sietse Ringers (ICTU), *De publieke
            voorbeeldwallet: techniek en context*
      - content: 15:15 - 15:45 pauze
      - content: 15:45 - 16:15 Job Doesburg (Radboud), *Maatregelen tegen overvraging*
      - content: |-
          16:15 - 16:45 Niels van Dijk (SURF), *SURF proof of concept met
            educatie wallet*
      - content: 16:45 - 17:00 Martijn Sanders (SIDN), *State of the IRMA*
      - content: "17:00: borrel, aangeboden door SURF"
  - title: "IRMA meeting: Vrijdagmiddag, 3 februari 2023 bij SURF"
    program:
      - content: 13:30 Opening, welkom
        files: []
      - content: 13:30 - 14:00 Wilbert Junte (Stichting CIS) en Bas Maat (Achmea), *IRMA
          & verzekeren*
      - content: 14:00 - 14:30 Tim Speelman (Beleidsmedewerker Digitale Identiteit,
          BZK), *Europese Digitale Identiteit en de publieke voorbeeldwallet*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=tqwLjATve5Q&list=PL4oSeW0AbF-kf39Wk5CKZ_D2RiHJQnNsq
      - content: |-
          14:30 - 15:00 Michiel Mayer (KvK) en Robert van Altena (VerID),
            *IRMA & KvK*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=ka8sjmsH3ZA&list=PL4oSeW0AbF-kf39Wk5CKZ_D2RiHJQnNsq
      - content: 15:00 - 15:30 Pauze
      - content: |-
          15:30 - 15:45 Gijs Nijman (Joindata), *Bedrijfsidentificatie in
            de agrisector*
      - content: |-
          15:45 - 16:00 Jos Kuijpers (Lead software developer, OnePlanet)
            *IRMA & OpenPlanet Data Platform*
      - content: 16:00 - 16:30 Martijn Sanders (SIDN), *State of the IRMA*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=_IzUOome1E4&list=PL4oSeW0AbF-kf39Wk5CKZ_D2RiHJQnNsq
      - content: 16:30 Borrel, aangeboden door SURF
  - title: "IRMA meeting: Vrijdagmiddag, 30 september 2022 bij SURF"
    program:
      - content: 13:30 Opening, welkom
      - content: |-
          13:30 - 14:00 Marlies Rikken, Niels van Dijk (SURF), *eduID en
            SSI volgens SURF*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=3ABM234pNzo
      - content: |-
          14:00 - 14:45 Laurens Debackere (Digitaal Vlaanderen), *An
            introduction to identity & authentication in the Flemish Solid
            ecosystem*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=jZIzLmnBB98
      - files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=6qHOe0ADd4I
        content: |-
          14:45 - 15:00 Daniel Ostkamp (RU), *Ontwikkeling Postguard email
            versleuteling*
      - content: 15:00 - 15:30 Pauze
      - content: 15:30 - 16:00 Bart Kerver (VWS), *UZI-pas ontwikkelingen*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=b-FPdFIgaoE
      - content: 16:00 - 16:15 Jan den Besten (RU), *IRMA in het nieuwe community
          netwerk PubHubs*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=HY7SyIHnIck
      - content: 16:15 - 16:30 Koen de Jonge (Procolix), *Met IRMA de cloud in*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=Kchnxjfa51I
      - content: 16:30 - 17:00 Martijn Sanders en Ivar Derksen (SIDN), *State of the
          IRMA*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=AAD6bJSRkho
      - content: 17:00 - 18:00 Borrel, aangeboden door SURF
  - title: "IRMA meeting: Vrijdagmiddag, 6 mei 2022 bij SURF"
    program:
      - content: 13:30 Opening, welkom
      - content: "Joost Fleuren (Kamer van Koophandel), *KVK bevoegdheden en IRMA:
          waarom en hoe?*"
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=IYckupZ3w10
      - content: 14:00 - 14:30 Allard Keuter (Signicat), *IRMA via de Signicat broker,
          voor eIDs in NL en EU*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=tWdr-UrN6Hg
      - content: 14:30 - 15:50 Daniel Ostkamp, Leon Botros (Radboud Universiteit),
          *Encrypted e-mail met IRMA, als minimal lovable product*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=tonNQQSPHiM
      - content: 15:50 - 15:10 Mike Alders (Gemeente Amsterdam), *Je identiteitsdocument
          in IRMA*. Als je je paspoort of ID-kaart meeneemt, kun je het zelf ook
          proberen
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=bPbg98nhdzI
      - content: 15:10 - 15:45 Pauze
      - content: 15:45 - 16:15 Ingrid Rijper, Anne van Doorn (StichtingCIS), *Ervaring
          met IRMA voor toegang tot centraal gedeelde verzekeringsgegevens*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=-BtVH4kigaE
      - content: 16:15 - 16:30 Wout Slakhorst (Nedap / Nuts), *Machtigen met KvK
          credential*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=GBcvpWHZ-NI
      - content: 16:30 - 17:00 Martijn Sanders, Sietse Ringers (SIDN), *State of the
          IRMA*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=8uWx5lzQvE0
      - content: 17:00 - 18:00 Borrel, aangeboden door SURF
  - description: De bijeenkomst begint om 13:30, in zaal 002 op Drift 25, van de
      Universiteit van Utrecht, op loopafstand van Centraal Station Utrecht.
    title: "IRMA meeting: Vrijdag middag 5 november 2021, bij de Universiteit Utrecht"
    program:
      - content: 13:30 Opening, welkom
      - content: "13:30 - 14:00 Remko Hoekstra en Ewout van Haeften (Centraal
          InkoopBureau, CIB). *Landelijke uitrol van de Gehandicapten Parkeer
          App: fraudebestrijding met behulp van IRMA*"
        files:
          - filename: "[ slides ]"
            file: /img/hoekstra-vanhaeften-gpa.pdf
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=Imrh_5nMSfU
      - content: 14:00 - 14:30 David Lamers en Joris Lange (Rabobank), *Deep dive &
          learnings from Datakeeper*
        files:
          - filename: "[ slides ]"
            file: /img/lamers-lange-datakeeper.pdf
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=QdIaK2X_5DM
      - content: 14:30 - 15:00 Pauze
      - content: "15:00 - 15:30 Dennis Bor en Lian Vervoort (Radboud Universiteit),
          *Twid: tegengaan van desinformatie op Twitter met IRMA*"
        files:
          - filename: "[ slides ]"
            file: /img/vervoort-bor-twid.pdf
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=KrXMINnrQHY
      - content: 15:30 - 16:00 Sietse Ringers en Martijn Sanders (SIDN), *State of the
          IRMA*
        files:
          - filename: "[ slides ]"
            file: /img/ringers-sanders-state.pdf
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=Y0SqLTmqtv4
      - content: 16:00 - 16:30 Peter Havekes (SURF - Trust and Identity), *Using IRMA as
          an alternative to in person identity validation*
        files:
          - filename: "[ slides ]"
            file: /img/havekes-vermeulen-rv.pdf
          - filename: "[ Video ]"
            file: https://www.youtube.com/watch?v=pl5Ho3llf9k
      - content: Ter afsluiting is er informeel napraten in The Streetfood
          Club,[](https://thestreetfoodclub.nl/) Janskerkhof 9, Utrecht.
  - title: "IRMA meeting: Vrijdag middag 25 juni 2021, online"
    program:
      - content: 14:00 Opening, welkom
      - content: 14:05 - 14:25 Marieke Brouwer (Gemeente Groningen) en Marloes van Unen
          (Gemeente Amsterdam), *IRMA-vote en pilots met burgerparticipatie in
          Groningen en Amsterdam*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=dDxw3Csd_2I
      - content: "14:25 - 14:40 Leon Botros en Daniel Ostkamp (RU), *Versleutelde e-mail
          met IRMA: eerste prototype*"
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=B6NDA1Ouq1k
      - content: "14:40 - 14:55 Rowan Goemans en Laura Loenen (RU), *Cryptify:
          versleutelde file transfer met IRMA*"
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=s3deWHy1uSc
      - content: 14:55 - 15:00 Koffie / thee / plas pauze
      - content: 15:00 - 15:20 Martijn van Dijk (RU, nu Gemeente Nijmegen),
          *Authenticiteit van berichten en hun afzender via IRMA handtekeningen
          als instrument tegen desinformatie*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=gb9qUjCHV04
      - content: 15:20 - 15:40 Reinder Rustema ([petities.nl](https://petities.nl/)),
          *Petities en initiatieven steunen met IRMA*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=ku5kxsoa2D4
      - content: 15:40 - 16:00 Ivar Derksen (SIDN), *State of the IRMA*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=OeT9WUs6Zsk
  - title: "IRMA meeting: Vrijdag middag 5 maart 2021, online"
    program:
      - content: 14:00 Opening
      - content: 14:05 Steven Gort (ICTU), *IRMA en Waardepapieren – Visie op attribuut
          gebasseerde credentials i.r.t. Publieke Dienstverlening*
        files:
          - filename: "[ slides ]"
            file: /img/kennissessie-irma-en-waardepapieren.pdf
          - filename: "[ Video ]"
            file: https://www.youtube.com/watch?v=lfWGalGOXXs
      - content: 14:25 Joost Fleuren (Innovatielab KvK), *Know Your Customer (KYC) check
          met delen bevoegdheden en machtigingen attributen, i.s.m. Ivido*
        files:
          - filename: "[ slides ]"
            file: /img/kvk-innovatielab-kyc.pdf
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=eJuFp9tBDVw
      - content: "Henk Dieter Oordt (Tweede Golf) en Lisa Bosma (Drechtsteden),
          *ID-contact: betere dienstverlening door jouw gemeente met meer grip
          op je gegevens*"
        files:
          - filename: "[ slides ]"
            file: /img/id-contact-irma-meetup.pdf
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=DqiHoqWpi5c
      - content: Marc De Jong (BZK), *Ontwikkelingen Digitale Overheid*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=lLodiKAWSPc
      - content: 15:20 Pauze
      - content: 15:30 Ineke van Gelder en Mike Alders (Team Digitale Identiteit
          Amsterdam), *Updates onderzoeken & next steps met IRMA in Amsterdam*
        files:
          - filename: "[ slides ]"
            file: /img/mijnamsterdam-en-filmpje.pdf
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=WyMgVToU5Cw
          - filename: "[ video: hoe werkt IRMA? ]"
            file: https://www.youtube.com/watch?v=iuGt-5UnNZs
      - content: 15:50 Bernard van Gastel (Radboud Universiteit), *Locaal stemmen met
          IRMA*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=wK-Aq4wDkw8
      - content: 16:10 Koen de Jonge (Procolix), *IRMA-meet*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=PGd78YDhMuQ
      - content: 16:25 Patrick Terranea (Zynyo), *IRMA-authenticatie voor document
          ondertekening*
        files:
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=7LyLsoIR1CE
      - content: 16:40 Sietse Ringers (SIDN), *Updates IRMA ontwikkelingen*
        files:
          - filename: "[ slides ]"
            file: /img/state-of-the-irma_5_3_2021.pdf
          - filename: "[ video ]"
            file: https://www.youtube.com/watch?v=PDACEbWLXVY
      - content: 16:55 Afsluiting en eind.
  - title: "IRMA meeting: vrijdag middag, 29 november 2019, te Amsterdam"
    description: "**This meeting is not in Utrecht**. This time it will be organised
      by the city of Amsterdam, at **Weesperstraat 113**, from 13:30 - 17:30."
    program:
      - content: 13:30 - 14:00 Sietse Ringers en Tomas Harreveld (Privacy by Design
          Foundation), *State of the IRMA*
        files:
          - filename: "[ slides ]"
            file: /img/ringers-harreveld-irma.pdf
      - content: 14:00 - 14:30 Mark Fonds, Ineke van Gelder, Mike Alders (Gemeente
          Amsterdam), *Inwoners centraal bij het ontwerp van nieuwe diensten*
        files:
          - filename: "[ slides ]"
            file: /img/amsterdam-inwonerscentraal-ontwerpen-ontwikkelen.pdf
      - content: 14:30 - 15:00 Hans Rob de Reus (Ministerie van Binnenlandse Zaken),
          *Proef met DigiD Bronidentiteit in combinatie met IRMA*
        files: []
      - content: 15:00 - 15:30 Pauze
      - content: 15:30 - 16:00 Alexander Blom (Bloqzone), *Veilig Bellen*
      - content: 16:00 - 16:30 Marcel Settels (Vereniging van Zorgaanbieders voor
          Zorgcommunicatie), *Proof of Concept Patientauthenticatie met IRMA*
        files:
          - filename: "[ slides ]"
            file: /img/vzvz_patientauthenticatie_poc.pdf
      - content: 16:30 - 17:00 Wouter Geraedts (Privacy by Design Foundation), *Identity
          based encryption of email with IRMA*
        files:
          - filename: "[ slides ]"
            file: /img/irmaseal-presentation.pdf
      - content: 17:00 - 17:30 Borrel
  - title: IRMA meeting, vrijdag middag 5 juli 2019, bij SURF
    program:
      - content: >-
          13:30 - 14:00 Sietse Ringers
          [](https://privacybydesign.foundation)(stichting Privacy by Design en
          Open Universiteit),  *Quick introduction to IRMA and overview of new

          developments*
        files:
          - filename: "[ slides ]"
            file: /img/ringers-harreveld-5-juli-2019.pdf
      - content: 14:00 - 14:30 Tristan Garssen, *Toepassingen van IRMA voor Persoonlijke
          Gezondheidsomgevingen (PGOs), bij Ivido en Medmij*
        files:
          - filename: "[ slides ]"
            file: /img/garssen-5-juli-2019.pdf
      - content: "14:30 - 15:00 Michiel Graat (SIDN),
          [](https://www.sidn.nl)Privacy-by-Design en SIDN samen op weg naar:
          Garantie op stabiliteit van de IRMA Backbone"
        files:
          - filename: "[ slides ]"
            file: /img/graat-5-juli-2019.pdf
      - content: 15:00 - 15:30 Pauze
      - content: 15:30 - 16:00 Mark Fonds (gemeente Amsterdam), *Een
          gebruiksvriendelijke gegevensmanager voor je online identiteit*
        files:
          - filename: "[ slides ]"
            file: /img/fonds-5-juli-2019.pdf
      - content: 16:00 - 16:30 Tomas Harreveld (stichting Privaccy by Design), I*RMA
          pilots bij verschillende gemeenten*
        files:
          - filename: "[ slides ]"
            file: /img/ringers-harreveld-5-juli-2019.pdf
      - content: 16:30 - 17:30 Borrel, aangeboden door SIDN
  - description: ""
    title: IRMA meeting,vrijdag middag, 8 maart 2019, bij SURF
    program:
      - content: 13:30 - 14:00 Sietse Ringers (stichting Privacy by Design), *Quick
          introduction to IRMA and overview of new developments*
        files:
          - filename: "[ slides ]"
            file: /img/ringers-8-maart-2019.pdf
      - content: 14:00 - 14:30 Niels Scheurleer (VGZ), *Machtiging via IRMA bij VGZ*
        files:
          - filename: "[ slides ]"
            file: /img/scheurleer-8-maart-2019.pdf
      - content: 14:30 - 15:00 Wout Slakhorst (Nedap / Nuts), *Authenticatie voor
          Persoonlijk Gezondheidsomgevingen (PGOs)*
        files:
          - filename: "[ slides ]"
            file: /img/slakhorst-8-maart-2019.pdf
      - content: 15:00 - 15:30
      - content: 15:30 - 16:00 Ivar Derksen (master student, Radboud University),
          *Back-up and recovery of IRMA attributes*
        files:
          - filename: "[ slides ]"
            file: /img/derksen-8-maart-2019.pdf
      - content: "16:00 - 16:30 Bram Withaar (gemeente Nijmegen), *Attribuut-uitgifte
          door gemeenten: status en plannen*"
        files:
          - filename: "[ slides ]"
            file: /img/withaar-8-maart-2019.pdf
      - content: 16:30 - 17:30 Borrel, aangeboden door SIDN
  - description: ""
    title: "IRMA meeting: vrijdag middag 1 juni 2018"
    program:
      - content: >-
          13:30 - 14:15 Sietse Ringers (Privacy by Design Foundation and

          Open University), *Introduction to IRMA and overview of new developments*
        files:
          - filename: "[ slides ]"
            file: /img/sietse-ringers-irma-developments.pdf
      - content: "14:15 - 15:00 Koen van Ingen (Alliander), IRMA signatures: sign-by-QR
          and sign-by-mail"
        files:
          - filename: "[ slides ]"
            file: /img/koen-van-ingen-signatures.pdf
      - content: 15:00 - 15:30 Pauze
      - content: 15:30 - 16:00 Timen Olthof (Alliander), *IRMA and blockchains*
        files:
          - filename: "[ slides ]"
            file: /img/timen-olthof-irma-schemes-blockchain.pdf
      - content: "16:00 - 16:30 Bram Withaar (gemeente Nijmegen), *IRMA for
          municipalities: issuance and verification of attributes*"
        files:
          - filename: "[ slides ]"
            file: /img/bram-withaar-nijmegen-issuance.pdf
      - content: 16:30 - 16:45 Bart Jacobs (stichting Privacy by Design Foundation en
          Radboud University), *Open discussion about strategies/next steps*
      - content: 16:45 - 17:30 Borrel
  - title: RMA meeting vrijdag ochtend 3 nov. 2017
    program:
      - content: 10.00 - 10:30 Oscar Koeroo (KPN information security office). *Security
          for Remote SIM Provisioning for e-SIM*
      - content: 10:30 - 11:00 Pim Vullers (NXP) *IRMA prototype on a NXP SmartMX3 P71*
          smart card
      - content: 11:00 - 11:30 Pauze
      - content: 11:30 - 12:00 Marie-José Hoefmans (Schluss). *The Schluss initiative*
        files:
          - filename: "[ slides ]"
            file: /img/schluss.pdf
      - content: Stijn Meijer (Radboud University en ING), *Comparing uPort’s
          blockchain-based identity management with IRMA*
      - content: |-
          12:15 - 12:45 Sietse Ringers (Radboud Universiteit), *Latest IRMA
          developments*
        files:
          - filename: "[ slides ]"
            file: /img/irma-developments-nov17.pdf
      - content: 12:45 Lunch
---
