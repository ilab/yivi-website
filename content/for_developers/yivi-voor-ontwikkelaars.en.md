---
layout: for_developers
title: Yivi for developers
sections:
  explanation:
    visibility: true
    title: Yivi explained for developers
    content: >-
      The idea of giving users control over their own data with open source
      software probably appeals to you. But how exactly does it work? We are
      happy to explain. For this, we have several platforms, some with more
      technical depth than others. You decide your deep dive.



      * Get inspired by [Yivi demo's](https://demos.yivi.app/).
    image: /img/developer-yivi.webp
    imageAltText: Yivi developer
  how_yivi_works:
    visibility: true
    title: How does Yivi work?
    content: Yivi is an app for your digital identity and personal data on your
      mobile phone. With Yivi, you easily and securely collect and share your
      data with services you like to use. For example, with Yivi you can
      identify yourself digitally to create an account, log in without a
      password, share data for an order or digitally sign a contract. Because
      the data comes from official data sources of the government or other
      organisations, as a service provider you can trust it and use it directly
      to optimise your services.
    image: /img/qr-codescannen-voor-yivi.webp
    imageAltText: Scanning Yivi QR-code
  role_of_parties:
    subtitle_issuer: Data source
    title_issuer: Issuer
    visibility: true
    title: What parties play a role?
    content: Besides the users, you also have an issuer and a verifier. We briefly
      explain the terms below.
    image_verifier: /img/verifier.webp
    imageAltText_verifier: user attribute verifier proof request
    content_verifier: If you fulfil the role of verifier then your organisation
      wants to know or verify something from your users. Users may or may not
      share desired data on request to log in or sign a digital signature, for
      example.
    title_verifier: Verifier
    image_issuer: /img/issuer.webp
    imageAltText_Issuer: user issuer request attribute
    subtitle_verifier: Data using services
    content_issuer: If you are an issuer then you are allowed to issue a specific
      attribute in the Yivi app. Think of BSN data, Chamber of Commerce number
      or customer number. Users can share this validated data via Yivi with
      different verifiers.
  wiki:
    visibility: true
    list_of_links:
      - devlink:
          subtitle: Wiki
          title: All technical documentation in a row
          content: The software on which Yivi runs is open source. The origins of Yivi lie
            with Radboud University Nijmegen. They developed the basis under the
            name IRMA. You will therefore still come across this name in the
            documentation with some regularity. Even the extensive technical
            documentation is still entirely written under the name IRMA. Here
            you will find all technical guidelines, api settings and other
            interesting documentation for setting up Yivi.
          imageAltText: Wiki
          image: /img/wiki.webp
          link_text: Visit our Wiki
          link: https://irma.app/docs/what-is-irma/
      - devlink:
          subtitle: GitHub
          title: Take a deep dive in the Open Source code
          content: On Github, you will find the code of the various components that make
            up Yivi. For each component, you can see which language is used in
            the code, such as, JavaScript, Go, Java, PHP and Swift. So if you
            really want to get started with the technically open source setup,
            Github is the place to find what you're looking for.
          image: /img/github_logo_png.webp
          imageAltText: GitHub logo
          link_text: Visit our GitHub
          link: https://github.com/privacybydesign
      - devlink:
          title: Discuss or ask your technical question
          subtitle: Slack
          content: Want to think along, discuss Yivi's technical applications? Then join
            the conversation on our [slack
            channel](https://irmacard.slack.com/). There you can also send your
            technical questions to our development team. Please drop us a line
            at [support@yivi.app](mailto:support@yivi.app) and we will add you
            to our slack channel.
          image: /img/slack_rgb.webp
          imageAltText: Slack logo
          link: https://irmacard.slack.com/
          link_text: Visit the slack community
---
