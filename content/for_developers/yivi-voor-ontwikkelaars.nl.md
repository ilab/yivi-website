---
layout: for_developers
title: Yivi voor ontwikkelaars
sections:
  explanation:
    visibility: true
    title: Yivi uitgelegd voor ontwikkelaars
    content: >
      
      Het idee om gebruikers zelf controle te geven over hun data met open source software spreekt je vast aan. Maar hoe werkt het precies? We leggen het graag uit. Hiervoor hebben we meerdere platformen, de ene met meer technische diepgang dan de ander. Jij bepaalt zelf je deep dive.



      * Laat je inspireren door [Yivi demo's](https://demos.yivi.app/).
    image: /img/developer-yivi.webp
    imageAltText: Yivi ontwikkelaar
  how_yivi_works:
    visibility: true
    title: Hoe werkt Yivi?
    content: >-
      Yivi is een app voor je digitale identiteit en persoonlijke gegevens op je
      mobiele telefoon. Met Yivi verzamel en deel je makkelijk en veilig je
      gevens met diensten die je graag gebruikt.


      Zo kun je met Yivi bijvoorbeeld jezelf digitaal identificeren voor het aanmaken van een account, inloggen zonder wachtwoord, gegevens delen voor een bestelling of digitaal een contract ondertekenen.


      Doordat de gegevens komen uit officiële databronnen van de overheid of andere organisaties, kun je als dienstverlener deze vertrouwen en direct gebruiken om je dienstverlening te optimaliseren.
    image: /img/qr-codescannen-voor-yivi.webp
    imageAltText: Yivi QR-code scannen
  role_of_parties:
    subtitle_issuer: Databron
    title_issuer: Issuer
    visibility: true
    title: Welke partijen spelen een rol?
    content: Naast de gebruikers heb je ook een issuer ofwel uitgever en een
      verifier ook wel controleur genoemd. Hieronder lichten we de begrippen
      kort toe.
    image_verifier: /img/verifier.webp
    imageAltText_verifier: user attribute verifier proof request
    content_verifier: Als je de rol vervult van verifier dan wil jouw organisatie
      iets willen weten of controleren van je gebruikers. Gebruikers delen op
      verzoek wel of niet de gewenste gegevens om bijvoorbeeld in te loggen of
      een digitale handtekening te zetten.
    title_verifier: Verifier
    image_issuer: /img/issuer.webp
    imageAltText_Issuer: user issuer request attribute
    subtitle_verifier: Data gebruikerende dienst
    content_issuer: Als je een issuer bent dan mag je een specifiek kenmerk uitgeven
      in de Yivi-app. Denk hierbij aan BSN-gegevens, KvK-nummer of klantnummer.
      Gebruikers kunnen deze gevalideerde gegevens via Yivi delen met
      verschillende verifiers.
  wiki:
    visibility: true
    list_of_links:
      - devlink:
          title: Alle technische documentatie op een rij
          subtitle: Wiki
          content: De software waarop Yivi draait is open-source. De oorsprong van Yivi
            ligt bij de Radboud Universiteit Nijmegen en Stichting Privacy by
            design. Zij ontwikkelden de basis onder de naam IRMA. Onder water
            kom je deze naam daarom nog in de documentatie met enige regelmaat
            tegen. Ook de uitgebreide technische documentatie is nog geheel
            geschreven onder de noemer IRMA. Je vindt hier alle technische
            richtlijnen, api-instellingen en andere interessante documentatie
            voor de inrichting van Yivi.
          image: /img/wiki.webp
          imageAltText: Wiki
          link_text: Ga naar onze Wiki
          link: https://irma.app/docs/what-is-irma/
      - devlink:
          title: Verdiep je in de Open Source code
          subtitle: GitHub
          content: Op Github vind je de code van de diverse onderdelen waaruit Yivi is
            opgebouwd. Privacy by Design is de founder van IRMA, de basis voor
            Yivi. Per onderdeel zie je aangegeven welke taal gebruikt wordt bij
            de code, zoals, JavaScript, Go, Java, PHP en Swift. Dus wil je echt
            aan de slag met de technisch open source inrichting dan is Github de
            plek waar je vindt wat je zoekt.
          image: /img/github_logo_png.webp
          imageAltText: GitHub logo
          link_text: Ga naar onze GitHub
          link: https://github.com/privacybydesign
      - devlink:
          title: Discussieer mee of stel je technische vraag
          subtitle: Slack
          content: Wil je meedenken, discussiëren over de technische toepassingen van
            Yivi? Praat dan mee op ons
            [slack-kanaal](https://irmacard.slack.com/). Daar kun je ook terecht
            voor je technische vragen aan ons developementteam. Stuur ons een
            mailtje via [support@yivi.app](mailto:support@yivi.app) en we voegen
            je toe.
          image: /img/slack_rgb.webp
          imageAltText: Slack logo
          link: https://irmacard.slack.com/
          link_text: Ga naar de slack community
---
