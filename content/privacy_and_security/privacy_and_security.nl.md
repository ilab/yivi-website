---
layout: privacy_and_security
title: Privacy en veiligheid
sections:
  privacy_and_security:
    title: Privacy en veiligheid
    content: "Met Yivi heb je de controle over je online gegevens. Jij beslist wat
      je opslaat, wat je deelt en met wie. De keuze is aan jou. Alles staat
      overzichtelijk bij elkaar, op een plek waar alleen jij bij kan: je eigen
      mobiel. Niemand die met je meekijkt. Zelfs Yivi niet."
    visibility: true
  complete_control:
    visibility: true
    title: Alles in eigen hand
    content: >-
      In de Yivi-app heb je altijd een duidelijke overzicht van je verzamelde
      persoonlijke gegevens. Ze staan alleen opgeslagen op je eigen mobiel. Je
      ziet ook wat je wanneer met wie gedeeld hebt. De organisatie achter Yivi
      ziet daar niks van. Ook kan niemand meekijken als je gegevens deelt met
      een andere partij. Je gegevens zijn van jou, en voor jou alleen.


      Raak je je telefoon kwijt? Dan kun je in een mum van tijd je Yivi-app blokkeren via Mijn Yivi. Zo kan nog steeds niemand bij jouw gegevens.
    image: /img/francisco-venancio-g_ov3t7wczg-unsplash.webp
    imageAltText: Man kijkt op zijn mobiel
  only_share_necessary:
    visibility: true
    title: Alleen delen wat écht nodig is
    content: Als je je aan iemand voorstelt, vertel je dan meteen je adres? Of je
      bankgegevens? Nee toch, want niet iedereen hoeft alles van je weten. Zo
      zou het online ook moeten werken. Toch deel je daar onbewust meer dan je
      denkt. Met allerlei bedrijven en instanties. Zonder dat je weet wat ze
      ermee doen. Moet je online aantonen dat je 18+ bent, dan hoef je geen
      geboortedatum of kopie paspoort meer te delen. Je deelt met de Yivi-app
      alleen een vinkje dat je 18+ bent, niets meer dan dat. Met Yivi heb jij de
      controle en deel je alleen nog wat écht nodig is.
    image: /img/ontmoetinng-in-park.webp
    imageAltText: Honden uitlaten in de park
  yivi_is_free:
    visibility: true
    title: Jouw gegevens zijn géén verdienmodel
    content: Yivi is gratis. Waar het addertje dan zit? Nou, die is er dus niet! Als
      Yivi-gebruiker betaal je niets. Alleen de aanbieders betalen om de open
      source software en infrastructuur van Yivi te gebruiken, zodat zij hun
      dienstverlening kunnen verbeteren.
  developed_by:
    visibility: true
    title: Vertrouwde oorsprong
    content: Yivi is in eerste vorm ontwikkeld aan de Radboud Universiteit Nijmegen.
      Yivi is in 2016 ondergebracht bij de onafhankelijke stichting Privacy by
      Design. Deze stichting heeft geen winstoogmerk en wil dat mensen met
      vertrouwen gebruik kunnen maken van privacy-vriendelijke digitale
      technologie, zonder dat ze met gerichte reclames en andere boodschapapen
      bestookt worden.
  take_back_control:
    visibility: true
    title: Pak de controle over je online gegevens
    content: >-
      Met Yivi kies jij bewust welke gegevens je deelt en met wie. Het zijn jouw
      gegevens, dus jij bepaalt. Ook online.




      **All you. All yours.**
    image: /img/jongen-met-yivi-app-1.jpg
    imageAltText: Man laat Yivi zien
---
