---
layout: privacy_and_security
title: Privacy and security
sections:
  privacy_and_security:
    title: Privacy and security
    content: "With Yivi you are in control of your digital data. You decide what to
      store, what to share, and who to share it with. It’s all up to you. Find
      everything in one place only you can access: your own phone. Without
      anyone watching. Not even Yivi."
    visibility: true
  complete_control:
    visibility: true
    title: Complete control
    content: >-
      Find a clear overview of your collected personal data in the Yivi app.
      Everything is stored locally on your own phone. You also see what you have
      shared with whom and when. The organisation behind Yivi does not see any
      of this. Also, no one can watch when you share data with another party.
      Your data belongs to you, and you alone.


      Did you lose your phone? Your Yivi app is blocked in a flash via My Yivi account. No one will ever be able to access what’s yours.
    image: /img/francisco-venancio-g_ov3t7wczg-unsplash.webp
    imageAltText: Man looks on his phone
  only_share_necessary:
    visibility: true
    title: Only share what’s absolutely necessary
    content: Would you give your address to a complete stranger? Or your credit card
      number? Of course not – it’s none of their business. It should work that
      way online too. But it doesn’t. In fact, you’re unknowingly sharing lots
      of stuff with different companies and organisations. What they do with
      your data is anyone’s guess. If you need to prove online that you are 18+,
      you no longer have to share a date of birth or a copy of your passport.
      All you share with the Yivi app is a checkmark that you are 18+, nothing
      more than that. With Yivi, you’re in charge. You only share what’s
      absolutely necessary.
    image: /img/ontmoetinng-in-park.webp
    imageAltText: Letting the dogs out in the park
  yivi_is_free:
    visibility: true
    title: No one should make money off your data
    content: Yivi is free. So what’s the catch? There is none – honestly. Yivi users
      don’t pay a single penny. The only ones who do are providers, so they can
      use Yivi's open source software and infrastructure, so they can improve
      their services.
  developed_by:
    visibility: true
    title: Trusted source
    content: In its first form Yivi was developed at Radboud University Nijmegen,
      The Netherlands. In 2016 it moved to the independent Privacy by Design
      Foundation. This foundation is non-profit and aims to enable people to use
      with confidence privacy-friendly digital technology, with being bombarded
      with targeted advertisements and other messages.
  take_back_control:
    visibility: true
    title: Take control of your digital data
    content: >-
      With Yivi, you choose which data you share, and who you share it with.
      It’s your data, so you’re in charge. On- and offline.




      **All you. All yours.**
    image: /img/jongen-met-yivi-app-1.jpg
    imageAltText: Man shows Yivi
---
